<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes(['verify' => true]);

Route::get('admin/login','Backend\AdminController@login')->name('admin.login');
Route::prefix('admin')->middleware(['auth','web','admin'])->namespace('Backend')->group(function () {
    Route::get('/','AdminController@index');
    Route::get('download','DownloadController@index');
    Route::get('stats','AdminController@stats');
    Route::get('settings','SettingController@show')->name('settings.index');
    Route::post('settings/store','SettingController@store')->name('settings.store');
    Route::resource('users','UserController')->except('show');
    Route::resource('matches','MatchController')->except('show');
    Route::resource('pages','PageController')->except('show');
    Route::resource('orders','OrderController')->except('show');
    Route::get('order/remove/{order_id}/{sku}','OrderController@productRemove');
    Route::get('order/add/{order_id}/{sku}','OrderController@productAdd');
    Route::get('order/update/{order_id}/{sku}','OrderController@productUpdate');
    Route::resource('ordertemplates','OrderTemplateController')->except('show');
});

// Frontend Routes
Route::namespace('Frontend')->middleware(['web'])->group(function () {
    Route::get('/','IndexController@index')->name('home.index');
    Route::get('elemzes/{slug}','IndexController@match');
    Route::post('payments/paypal/create', 'PaymentController@createPaypal');
    Route::post('payments/paypal/backref/{orderID?}', 'PaymentController@getPaypal');
    Route::get('thankyou', 'PaymentController@thankyou');
    Route::get('cancel', 'PaymentController@cancel');

    Route::middleware(['auth'])->group(function () {
        Route::get('elofizetes', 'PaymentController@pay');
        Route::get('payments/paypal/create', 'PaymentController@createPaypal');
        Route::get('profile', 'ProfileController@index')->name('profile');
        Route::post('profile/update', 'ProfileController@update')->name('profile.update');
        Route::post('profile/password/change', 'ProfileController@changePassword')->name('profile.passwordchange');
        Route::delete('profile/delete', 'ProfileController@delete')->name('profile.delete');
    });

    Route::get('felhasznalasi-feltetelek', function () {
        return view('frontend.info.aszf.v10');
    });

    Route::get('adatkezelesi-tajekoztato', function () {
        return view('frontend.info.adatvedelem.v10');
    });

    Route::get('fizetesi-tajekoztato', function () {
        return view('frontend.info.fizetes.v10');
    });

    Route::get('gyik', function () {
        return view('frontend.info.gyik.gyik');
    });
    Route::get('kapcsolat', function () {
        return view('frontend.info.kapcsolat.kapcsolat');
    });
});
