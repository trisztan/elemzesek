@extends('frontend.frontend')

@section('content')
    <div class="topic">
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <h3>Regisztráció</h3>
                </div>
                <div class="col-sm-8">
                    <ol class="breadcrumb pull-right hidden-xs">
                        <li><a href="{{url('/')}}">Főoldal</a></li>
                        <li class="active">Regisztráció</li>
                    </ol>
                </div>
            </div> <!-- / .row -->
        </div> <!-- / .container -->
    </div>
    <section class="shop-index__section">
    <div class="container pb-5">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">

                <div class="sign__form">

                    <h3 class="sign-form__title">Bejelentkezés</h3>

                    <form method="POST" action="{{ route('register') }}">
                    @csrf
                    <!-- Lastname -->
                        <div class="form-group">
                            <label for="name" class="sr-only">Felhasználónév</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <input  id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" placeholder="Felhasználónév" required autofocus>
                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <!-- Lastname -->
                        <div class="form-group">
                            <label for="lastname" class="sr-only">Vezetéknév</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <input  id="lastname" type="text" class="form-control{{ $errors->has('lastname') ? ' is-invalid' : '' }}" name="lastname" value="{{ old('lastname') }}" placeholder="Vezetéknév" required autofocus>
                                @if ($errors->has('lastname'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('lastname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <!-- / .form-group -->
                        <!-- Lastname -->
                        <div class="form-group">
                            <label for="firstname" class="sr-only">Keresztnév</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <input  id="firstname" type="text" class="form-control{{ $errors->has('firstname') ? ' is-invalid' : '' }}" name="firstname" value="{{ old('firstname') }}" placeholder="Keresztnév" required autofocus>
                                @if ($errors->has('firstname'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('firstname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <!-- / .form-group -->
                        <!-- Email -->
                        <div class="form-group">
                            <label for="email" class="sr-only">E-mail cím</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <input  id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="E-mail cím" required autofocus>
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <!-- / .form-group -->
                        <!-- Password -->
                        <div class="form-group">
                            <label for="password" class="sr-only">Jelszó</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Jelszó">
                            </div>
                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                        </div> <!-- / .form-group -->
                        <!-- Password -->
                        <div class="form-group">
                            <label for="password_confirmation" class="sr-only">Jelszó ismét</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                <input id="password_confirmation" type="password" class="form-control{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}" name="password_confirmation" placeholder="Jelszó ismét">
                            </div>
                            @if ($errors->has('password_confirmation'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                            @endif
                        </div> <!-- / .form-group -->
                        <!-- Remember me -->
                        <div class="form-group">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="subscribe" id="subscribe"  {{ old('subscribe') ? 'checked' : '' }}> Felíratkozás a hírlevéle
                                </label>
                            </div>
                        </div> <!-- / .form-group -->
                        <!-- Submit -->
                        <button type="submit" class="btn btn-primary">Regisztráció</button>
                        <hr>
                    </form>

                    <!-- Sign up link -->
                    <p>Van már fiókod? <a href="{{url('login')}}">Belépés</a></p>
                </div> <!-- / .sign__form -->
            </div>
        </div> <!-- / .row -->
    </div>
    </section>
@endsection