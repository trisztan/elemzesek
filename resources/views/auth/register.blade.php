@extends('layouts.frontend')

@section('content')
<!-- Start: Features Section 1
====================================== -->
<section class="features-section-1 relative background-semi-dark" id="features">
    <div class="container">
        <div class="row section-separator">

            <!-- Start: Section Header -->
            <div class="section-header col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">

                <h2 class="section-heading">Regisztráció</h2>
                <p class="sub-heading">A regisztráció teljesen ingyenes.</p>
            </div>
            <!-- End: Section Header -->

            <div class="clearfix"></div>

            <div class="col-xs-12 features-item">
       <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Felhasználónév <span class="badge" data-toggle="tooltip" title=" A Felhasználónévnek legalább 5 karakterből kell állnia."><i class="fa fa-info"></i></span></label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{old('name') }}" required autofocus">

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
    
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-mail cím <span class="badge" data-toggle="tooltip" title="Érvényes e-mail megadása kötelező"><i class="fa fa-info"></i></span></label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{old('email') }}" required autofocus">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Jelszó <span class="badge" data-toggle="tooltip" title=" A jelszónak legalább 8 karakterből kell állnia, és tartalmaznia kell kis- és nagybetűt, valamint számot"><i class="fa fa-info"></i></span></label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Jelszó ismét <span class="badge" data-toggle="tooltip" title="A megerősítéshez írd be ismét a jelszót"><i class="fa fa-info"></i></span></label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('accept') ? ' has-error' : '' }}">
                            <div class="col-md-offset-2 col-md-8">
                                <div class="checkbox-animated">
                                    <input id="checkbox_animated_1" type="checkbox" name="accept">
                                    <label for="checkbox_animated_1">
                                        <span class="check"></span>
                                        <span class="box"></span>
                                        Elolvastam és elfogadtam az <a href="{{ url('felhasznalasi-feltetelek') }}">Általános Szerződési feltételeket</a>,
                                        <a href="{{ url('adatkezelesi-tajekoztato') }}">Adatkezelési tájékoztatót</a>,
                                        <a href="{{ url('fizetes-tajekoztato') }}">Fizetési tájékoztatót</a> 
                                    </label>
                                </div>
                                @if ($errors->has('accept'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('accept') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Regisztráció
                                </button>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <a href="{{ url('/password/reset') }}">
                                    Elfelejtetted a jelszavadat?
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
            </div>

        </div> <!-- End: .row -->
    </div> <!-- End: .container -->
</section>
<!-- End: Features Section 1
======================================-->
@endsection
