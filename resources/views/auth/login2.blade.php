@extends('frontend.frontend')

@section('content')
    <div class="topic">
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <h3>Bejelentkezés</h3>
                </div>
                <div class="col-sm-8">
                    <ol class="breadcrumb pull-right hidden-xs">
                        <li><a href="{{url('/')}}">Főoldal</a></li>
                        <li class="active">Bejelentkezés</li>
                    </ol>
                </div>
            </div> <!-- / .row -->
        </div> <!-- / .container -->
    </div>
    <section class="shop-index__section">
    <div class="container pb-5">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">

                <div class="sign__form">

                    <h3 class="sign-form__title">Bejelentkezés</h3>



                    <!-- Sign up link -->
                    <p>Nem regisztráltál még? <a href="sign-up.html">Regisztrálj most.</a></p>

                    <p>
                        Elfelejtetted a jelszavadat?
                        <a href="#lost-password__form" data-toggle="collapse" aria-expanded="false" aria-controls="lost-password__form">
                            Kattints ide a visszaállításhoz
                        </a>
                    </p>

                    <div class="collapse" id="lost-password__form">

                        <p class="text-muted">
                            Enter your email address below and we will send you a link to reset your password.
                        </p>

                        <!-- Lost password -->
                        <form class="form-inline">
                            <div class="form-group">
                                <label class="sr-only" for="lost-password__email">Email address</label>
                                <input type="email" class="form-control" id="lost-password__email" placeholder="Enter email">
                            </div>
                            <button type="submit" class="btn btn-secondary">Send</button>
                        </form>

                    </div> <!-- lost-password__form -->

                </div> <!-- / .sign__form -->

            </div>
        </div> <!-- / .row -->
    </div>
    </section>
@endsection
