<!DOCTYPE html>
<html lang="">
<head>
    <!-- TITLE OF SITE -->
    <title>{{ config('sitename') }}</title>

    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="{{ config('sitename') }}"/>
    <meta name="keywords" content="{{ config('sitename') }}"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{ mix('frontend/css/app.css') }}">
{!!html_entity_decode(SEO::generate())!!}




<!-- FAV AND TOUCH ICONS   -->
    <link rel="icon" href="images/favicon.ico">
    <link rel="apple-touch-icon" href="images/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png">

    <!-- GOOGLE FONTS -->
    <link href='https://fonts.googleapis.com/css?family=Lato:400,100,300,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Poppins:400,300,500,600' rel='stylesheet' type='text/css'>

    <meta name="google-site-verification" content="{{ env('GOOGLE_SEARCHCONSOLE_CODE') }}"/>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->


    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.6/cookieconsent.min.css">
@yield('header')
<!-- Google Tag Manager -->
    <script>(function (w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start':
                    new Date().getTime(), event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-PSBS87G');</script>
    <!-- End Google Tag Manager -->
    <script id="Cookiebot" src="https://consent.cookiebot.com/uc.js" data-cbid="c1d4c770-5f6c-478b-bafa-e447fe29a9c4" type="text/javascript" async></script>
</head>
<body>
<div class="preloader">
    <div class="loader"></div>
</div>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PSBS87G"
            height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->
<div id="fb-root"></div>
<script>(function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = 'https://connect.facebook.net/hu_HU/sdk.js#xfbml=1&version=v2.11&appId=342141409191570';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<nav class="navbar navbar-default navbar-fixed-top background-green">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{url('/')}}"><img src="{{ url('images/logo.png') }}" alt=""
                                                             class="logo-1 img-responsive"></a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">

                @if(!Auth::user())
                    {{-- <li><a class="btn-warning" href="{{ url('elofizetes') }}"><i class="fa fa-star"></i> Előfizetés</a>
                     </li>--}}
                    <li><a class="btn-info" href="{{ url('login') }}"><i class="fa fa-user"></i> Belépés</a></li>
                    <li><a class="btn-danger" href="{{ url('register') }}"><i class="fa fa-plus"></i> Regisztráció</a>
                    </li>
                @else
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                           aria-expanded="false">
                            <i class="fa fa-star"></i> Üdv, {{ Auth::user()->name }}
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            @if (Auth::user()->is_admin == 1)
                                <li><a href="{{ url('admin') }}">Admin</a></li>
                            @endif
                            <li><a href="{{ url('profile') }}">Profil</a></li>
                            <li>  <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                   Kilépés
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form></li>

                        </ul>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</nav>
@include('flash::message')
@yield('content')
<footer class="footer-section background-green">
    <div class="container">
        <div class="row">

            <div class="footer-header">
                <div class="section-separator">
                    <div class="each-section col-sm-3 col-xs-12">
                        <ul class="nav link-list">
                            <li><a href="{{ url('adatkezelesi-tajekoztato') }}">Adatkezelési tájékoztató</a></li>
                            <li><a href="{{ url('felhasznalasi-feltetelek') }}">Felhasználási feltételek</a></li>
                            <li><a href="{{ url('fizetesi-tajekoztato') }}">Fizetési tájékoztató</a></li>
                            <li> <a href="javascript: Cookiebot.renew()">Süti beállítások</a></li>
                            <li><a href="{{ url('kapcsolat') }}">Kapcsolat</a></li>
                            <li><a href="{{ url('gyik') }}">Gyakori kérdések</a></li>
                        </ul>
                    </div> <!-- End: .each-section -->
                    <div class="each-section col-sm-3 col-xs-12">
                        <ul class="nav link-list">
                            <li><a href="{{ url('login') }}">Bejelentkezés</a></li>
                            <li><a href="{{ url('register') }}">Regisztráció</a></li>
                            <li><a href="{{ url('password/reset') }}">Elfelejtett jelszó</a></li>
                        </ul>
                    </div> <!-- End: .each-section -->
                    <div class="each-section col-sm-3 col-xs-12">
                        <ul class="nav link-list">
                            <li><a href="#">Partnereink</a></li>
                            <li><a href="http://www.livescore.in/hu/" title="Livescore.in" target="_blank">Livescore.in</a></li>
                            <li><a href="http://www.livescore.in/hu/"  title="Livescore.in" target="_blank"><img src="{{ asset('images/partners/livescore_in.png') }}"></a></li>
                            <li><a href="https://barion.hu"><img src="{{ asset('images/partners/barion_hu.png') }}"></a></li>
                        </ul>
                    </div> <!-- End: .each-section -->
                    <div class="each-section vertical-bottom col-sm-3 col-xs-12">

                        <div class="fb-page" data-href="https://www.facebook.com/elemzesek" data-tabs="timeline"
                             data-height="130" data-small-header="false" data-adapt-container-width="true"
                             data-hide-cover="false" data-show-facepile="true">
                            <blockquote cite="https://www.facebook.com/elemzesek" class="fb-xfbml-parse-ignore"><a
                                    href="https://www.facebook.com/elemzesek">Elemzések</a></blockquote>
                        </div>
                    </div> <!-- End: .each-section -->

                </div>
            </div> <!-- End: .footer-header -->
            @if(config('settings.premium'))
                <div class="copyright text-center col-xs-12">
                    <p>{{ config('settings.copyright') }}</p>
                    <img src="{{ asset('images/barion.png') }}">
                </div> <!-- End: .copyright -->
            @endif
        </div><!-- End: .section-separator  -->
    </div> <!-- End: .container  -->
</footer>
<!-- End: Footer Section 1
================================== -->


<script src="//code.jquery.com/jquery-3.3.1.min.js"></script>
<!-- CDN -->

<script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.6/cookieconsent.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
        integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/5c8cfe43101df77a8be2e69d/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
    })();
</script>
<!--End of Tawk.to Script-->
<!-- Hotjar Tracking Code for https://elemzesek.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:1240692,hjsv:6};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
</script>
<!-- Scripts -->
<script src="{{ mix('frontend/js/app.js') }}"></script>
@yield('footer')
</body>

</html>
