@extends('frontend.frontend')

@section('content')

    <!-- Start: Header Section
================================ -->

    <section class="header-section-1 bg-image-1 header-js">
        <div class="container">
            <div class="row section-separator">

                <div class="col-md-6 col-sm-12">
                    <div class="part-inner text-center">
                    @if(!Auth::user())
                        <!--  Header SubTitle Goes here -->
                            <h1 class="title"></h1>

                            <div class="detail">
                                <p>Üdvözlünk!<p>
                                <p>Elindultunk! Az elemzesek.com egy legális meccselemző weblap minden napra meccselemzéseket készítünk és kiválasztjuk a legjobb tippeket!</p>
                                <p>Segítünk dönteni ha bizonytalan van, vagy csak ritkán fogadsz és nem követed a csapatokat.</p>
                                <p>Szeretnénk egy színvonalas szolgáltatást nyújtani, idővel minden bajnoksághoz dedikált elemzőt szerezni, és mobil alkalmazást is!</p>
                                <p>A 30 napos előfizetésre jogszabály szerint 14 napos pénz visszafizetési garanciát és számlát is adunk.</p>
                                <p>Az előfizetéssel támogatod napi 10-12 órás munkánkat és a weboldal fejlődését! Köszönjük!</p>
                            </div>
                            <a href="{{ url('register') }}" class="btn btn-success btn-block btn-lg m-t-30">Ingyenes
                                Regisztráció</a>
                            <br/>
                            {{--<!--<div class="detail">
                               <p>Minden elemzéshez a legjobb tippeket adjuk.</p>
                           </div>
                          <a href="{{ url('elofizetes') }}" class="btn btn-warning btn-block btn-lg m-b-10">Előfizetés
                               most csak <b>{{ config('settings.pricesale') }}</b> Ft/hó
                               <s><b>{{ config('settings.price') }} Ft</b> helyett</s></a>--}}

                        @else
                            <h1 class="title">Üdv, {{ Auth::user()->name }} </h1>
                            @if(Auth::user()->access == true)
                                <div class="detail">
                                    <p>Előfizetésed lejár: </p>
                                    <p class="count">{{ Auth::user()->expired_at }}</p>
                                    <p class="count">Még {{ Auth::user()->access_days }} nap van hátra.</p>
                                </div>
                            @else
                                <div class="detail">
                                    <p>Üdvözlünk!<p>
                                    <p>Elindultunk! Az elemzesek.com egy legális meccselemző weblap minden napra meccselemzéseket készítünk és kiválasztjuk a legjobb tippeket!</p>
                                    <p>Segítünk dönteni ha bizonytalan van, vagy csak ritkán fogadsz és nem követed a csapatokat.</p>
                                    <p>Szeretnénk egy színvonalas szolgáltatást nyújtani, idővel minden bajnoksághoz dedikált elemzőt szerezni, és mobil alkalmazást is!</p>
                                    <p>A 30 napos előfizetésre jogszabály szerint 14 napos pénz visszafizetési garanciát és számlát is adunk.</p>
                                    <p>Az előfizetéssel támogatod napi 10-12 órás munkánkat és a weboldal fejlődését! Köszönjük!</p>
                                </div>

                                <a href="{{ url('elofizetes') }}" class="btn btn-warning btn-block btn-lg m-t-30">Előfizetés
                                    most csak <b>{{ \App\Models\Setting::getValue('saleprice') }}</b> Ft/hó
                                    <s><b>{{App\Models\Setting::getValue('price') }} Ft</b> helyett</s></a>

                            @endif
                        @endif
                    </div>
                </div> <!-- End: .part-1 -->
                <div class="col-md-6 hidden-sm hidden-xs">
                    <div class="count-down col-xs-12">
                        <div class="row">
                            <div class="each-item col-sm-6 col-xs-12">
                                <div class="inner">
                                    <div class="group">
                                        <i class="fa fa-user fa-4x"></i>
                                        <h4 class="number">{{ $usersSimple }}</h4>
                                        <p class="title">Felhasználók</p>

                                    </div>
                                </div>
                            </div> <!-- End: .each-item -->

                            <div class="each-item col-sm-6 col-xs-12">
                                <div class="inner">
                                    <div class="group">
                                        <i class="fa fa-star fa-4x"></i>
                                        <h4 class="number">{{ $usersPremium }}</h4>
                                        <p class="title">Prémium előfizető</p>

                                    </div>
                                </div>
                            </div> <!-- End: .each-item -->

                            <div class="each-item col-sm-6 col-xs-12">
                                <div class="inner">
                                    <div class="group">
                                        <i class="fa fa-users fa-4x"></i>
                                        <h4 class="number">{{ $matchesTotal }}</h4>
                                        <p class="title">Összes elemzés</p>
                                    </div>
                                </div>
                            </div> <!-- End: .each-item -->
                            <div class="each-item col-sm-6 col-xs-12">
                                <div class="inner">
                                    <div class="group">
                                        <i class="fa fa-list fa-4x"></i>
                                        <h4 class="number">{{ $matchesToday }}</h4>
                                        <p class="title">Összes mai elemzés</p>
                                    </div>
                                </div>
                            </div> <!-- End: .each-item -->

                        </div> <!-- End: .row -->
                    </div> <!-- End: .col-xs-12 -->
                </div> <!-- End: .part-1 -->
            </div> <!-- End: .row -->
        </div> <!-- End: .container -->
    </section>

    <!-- End: Header Section
    ================================ -->

    <!-- Start: Features Section 1
    ====================================== -->
    <section class="matches-v2">
        <div class="container">
            <div class="row section-separator">
                <div class="col-md-12">
                    <div class="kode-fixer-list">
                        <ul class="table-head thbg-color">
                            <li><h5 class="hidden-xs hidden-sm">Legfrissebb elemzések</h5><h5 class="hidden-md hidden-lg">Meccs</h5></li>
                            <li class="hidden-xs hidden-sm"><h5 class="hidden-xs hidden-sm">Esemény</h5></li>
                            <li class="width:50px"></li>
                            <!--<li class="fixer-pagination"> <a href="#" class="fa fa-angle-right"></a> <a href="#" class="fa fa-angle-left"></a> </li>-->
                        </ul>
                        @if(count($matches) > 0)
                            @foreach($matches as $match)
                                <ul class="table-body">
                                    <li>
                                        <a href="{{url('elemzes/'.$match->slug)}}" class="list-thumb">
                                            @if($match->team_logo)
                                                <img width="50" src="{{asset('storage/logo/'. $match->team_logo)}}">
                                            @endif
                                            {{str_limit($match->team,12,'...')}}
                                        </a>
                                        <span>vs</span>
                                        <a href="{{url('elemzes/'.$match->slug)}}" class="list-thumb">
                                            @if($match->team2_logo)
                                                <img width="50" src="{{ asset('storage/logo/'.$match->team2_logo)}}">
                                            @endif
                                            {{str_limit($match->team2,12,'...')}}
                                        </a>
                                    </li>
                                    <li class="hidden-sm hidden-xs">
                                        @if($match->started_at_second != 0)
                                            <small>{{ $match->started_at }}</small>
                                            <div class="timer-quick" data-seconds-left="{!! $match->started_at_second  !!}"></div>
                                        @else
                                            <div class="started">Elkezdődött</div>
                                        @endif
                                        <small>{{ $match->league }}</br>{{ $match->stadium }}</small>
                                    </li>
                                    <li class="fixer-btn">
                                        @if($match->free == true)
                                            <a class="btn btn-success" href="{{ url('elemzes/'.$match->slug) }}">Megtekintés</a></a>
                                        @else
                                            @if(Auth::user())
                                                @if(Auth::user()->access == true)
                                                    <a class="btn btn-success"
                                                       href="{{ url('elemzes/'.$match->slug) }}">Megtekintés</a>
                                                @else
                                                    <a class="btn btn-warning" href="{{ url('elofizetes') }}">Előfizetés
                                                        szükséges</a>
                                                @endif
                                            @else
                                                <a class="btn btn-danger text-center"
                                                   href="{{ url('login') }}">Belépés</a><a class="btn btn-danger"
                                                                                           href="{{ url('register') }}">Regisztráció</a>
                                            @endif
                                        @endif
                                    </li>
                                </ul>
                            @endforeach
                        @else
                            <ul>
                                <li class="text-center no-match">Mára még nincs elemzés, de nézz vissza később.</li>
                            </ul>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        </div>
        <!-- End: Features Section 1
        ======================================-->
@endsection
