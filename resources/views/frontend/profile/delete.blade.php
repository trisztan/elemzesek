Figyelem minden adatod törlésre kerül!

<form action="{{ route('profile.delete') }}" method="POST">
    @csrf
    @method('DELETE')
    <button type="submit" class="btn btn-danger">Profil törlése</button>
</form>
