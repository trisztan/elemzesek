{{ Form::model(auth()->user(), ['route' => 'profile.update', 'class' => 'form-horizontal', 'method' => 'POST']) }}
<div class="form-group row">
    <label for="name" class="col-md-4 col-form-label">Felhasználónév</label>

    <div class="col-md-8">
        <input id="name" type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="name" value="{{ auth()->user()->name ?? old('name') }}" placeholder="Felhasználónév" required autofocus>
        @if ($errors->has('name'))
            <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('name') }}</strong>
        </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label for="lastname" class="col-md-4 col-form-label">Vezetéknév</label>
    <div class="col-md-8">
        <input id="lastname" type="text" class="form-control{{ $errors->has('lastname') ? ' is-invalid' : '' }}" name="lastname" value="{{ auth()->user()->lastname ?? old('lastname') }}" placeholder="Vezetéknév" required autofocus>
        @if ($errors->has('lastname'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('lastname') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label for="firstname" class="col-md-4 col-form-label">Keresztnév</label>
    <div class="col-md-8">
        <input id="firstname" type="text" class="form-control{{ $errors->has('firstname') ? ' is-invalid' : '' }}" name="firstname" value="{{ auth()->user()->firstname ?? old('firstname') }}" placeholder="Vezetéknév" required autofocus>
        @if ($errors->has('firstname'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('firstname') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label for="email" class="col-md-4 col-form-label">E-mail cím</label>

    <div class="col-md-8">
        <input id="email" type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ auth()->user()->email ?? old('email') }}" placeholder="E-mail cím" required autofocus>
        @if ($errors->has('email'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label for="mobile" class="col-md-4 col-form-label">Telefonszám</label>

    <div class="col-md-8">
        <input id="mobile" type="text"  class="form-control{{ $errors->has('mobile') ? ' is-invalid' : '' }}" name="mobile" value="{{ auth()->user()->mobile ?? old('mobile') }}" placeholder="Telefonszám" required autofocus>
        @if ($errors->has('mobile'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('mobile') }}</strong>
            </span>
        @endif
    </div>
</div>
<h4>Számlázási cím</h4>
    <div class="form-group row">
        <label for="zip" class="col-md-4 col-form-label">Irányítószám</label>

        <div class="col-md-8">
            <input id="zip" type="text" class="form-control{{ $errors->has('zip') ? ' is-invalid' : '' }}" name="zip" value="{{ auth()->user()->zip ?? old('zip') }}" placeholder="Irányítószám" required autofocus>
            @if ($errors->has('zip'))
                <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('zip') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="form-group row">
        <label for="name" class="col-md-4 col-form-label">Város</label>

        <div class="col-md-8">
            <input id="city" type="text" class="form-control{{ $errors->has('city') ? ' is-invalid' : '' }}" name="city" value="{{ auth()->user()->city ?? old('city') }}" placeholder="Város" required autofocus>
            @if ($errors->has('city'))
                <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('city') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="form-group row">
        <label for="name" class="col-md-4 col-form-label">Utca, házszám</label>

        <div class="col-md-8">
            <input id="street" type="text" class="form-control{{ $errors->has('street') ? ' is-invalid' : '' }}" name="street" value="{{  auth()->user()->street ?? old('street') }}" placeholder="Utca, házszám, emelet, ajtó" required autofocus>
            @if ($errors->has('street'))
                <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('street') }}</strong>
            </span>
            @endif
        </div>
    </div>
<div class="form-group row">
    <div class="col-md-8 ml-auto">
        <button class="btn btn-primary">Mentés</button>
    </div>
</div>

{{ Form::close() }}
