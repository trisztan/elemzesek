{{ Form::open(['route' => ['profile.passwordchange'], 'class' => 'form-horizontal', 'method' => 'POST']) }}

<div class="form-group row">
    <label for="old_password" class="col-md-4 col-form-label">Régi jelszó</label>

    <div class="col-md-8">
        <input id="old_password" type="text" class="form-control{{ $errors->has('old_password') ? ' is-invalid' : '' }}" name="old_password"  placeholder="Régi jelszó" required autofocus>
        @if ($errors->has('firstname'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('firstname') }}</strong>
            </span>
        @endif
        @if ($errors->has('old_password'))
            <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('old_password') }}</strong>
        </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label for="password" class="col-md-4 col-form-label">Új jelszó</label>

    <div class="col-md-8">
        <input id="password" type="text" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Új jelszó" required autofocus>
        @if ($errors->has('password'))
            <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('password') }}</strong>
        </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label for="password_confirmation" class="col-md-4 col-form-label">Új jelszó ismét</label>

    <div class="col-md-8">
        <input id="password_confirmation" type="text" class="form-control{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}" name="password_confirmation"  placeholder="Új jelszó ismét" required autofocus>
        @if ($errors->has('password_confirmation'))
            <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('password_confirmation') }}</strong>
        </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <div class="col-md-8 ml-auto">
        <button class="btn btn-primary">Mentés</button>
    </div>
</div>

{{ Form::close() }}
