@extends('frontend.frontend')

@section('content')
<!-- Start: Features Section 1
====================================== -->
<section class="features-section-1 relative background-semi-dark" id="features">

    <div class="container">
        <div class="row section-separator">

            <!-- Start: Section Header -->
            <div class="section-header col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">

                <h2 class="section-heading">Bejelentkezés</h2>

            </div>
            <!-- End: Section Header -->

            <div class="clearfix"></div>

            <div class="col-xs-12 features-item">
     <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-mail cím</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="E-mail cím" required autofocus>
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Jelszó</label>

                            <div class="col-md-6">

                                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Jelszó">

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" id="remember"  {{ old('remember') ? 'checked' : '' }}> Megjegyzés
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Bejelentkezés
                                </button>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <a href="{{ url('/password/reset') }}">
                                    Elfelejtetted a jelszavadat?
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
            </div>

        </div>
    </div>
</section>
@endsection
