@extends('frontend.frontend')

@section('title', __('labels.user.titles.account'))

@section('content')
    <section class="header-section-1">
        <div class="container">
            <div class="row section-separator">

    <div class="card">
        <div class="card-header">
            <ul class="nav nav-tabs card-header-tabs" role="tablist">
                {{--<li class="nav-item">
                   <a class="nav-link" data-toggle="tab" href="#profile" role="tab">@lang('labels.user.profile')</a>
               </li>--}}

               <li class="nav-item">
                   <a class="nav-link" data-toggle="tab" href="#edit" role="tab">Személyes adatok</a>
               </li>

               <li class="nav-item">
                   <a class="nav-link" data-toggle="tab" href="#password" role="tab">Jelszó</a>
               </li>

               <li class="nav-item">
                   <a class="nav-link" data-toggle="tab" href="#delete" role="tab">Profil törlése</a>
               </li>

           </ul>
       </div>
       <div class="card-clock">
           <div class="tab-content p-3">

                <div role="tabpanel" class="tab-pane active" id="edit">
                    @include('frontend.profile.edit')
                </div>

                <div role="tabpanel" class="tab-pane" id="password">
                    @include('frontend.profile.password')
                </div>


                <div role="tabpanel" class="tab-pane" id="delete">
                    @include('frontend.profile.delete')
                </div>

            </div>
        </div>
    </div>
            </div>
        </div>
    </section>
@endsection
