@extends('frontend.frontend')

@section('content')
    <section class="features-section-1 relative background-semi-dark" id="features">
        <div class="container">
            <div class="row section-separator">
                <h2>Kapcsolat</h2>

                Online Chat a jobb alsó sarokban vagy e-mail: info@elemzesek.com
            </div>
        </div>
    </section>
@endsection
