@extends('frontend.frontend')

@section('content')
    <section class="features-section-1 relative background-semi-dark" id="features">
        <div class="container">
            <div class="row section-separator">
                <object width="100%" height="3000" type="application/pdf" data="{{asset('pdf/aszf_2019_03_16.pdf')}}?#zoom=140&scrollbar=0&toolbar=0&navpanes=0">
                    <embed src="{{asset('pdf/aszf_2019_03_16.pdf')}}" type="application/pdf" />
                </object>
            </div>
        </div>
    </section>
@endsection
