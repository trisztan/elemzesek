<div class="match-banner sub-title">
    <div class="container">
        <div class="text-center">
            <h2 class="match-detail-league-title"><a href="#">{{ $match->league }}  {{ $match->stadion }}</a></h2>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-4 matech-team-left">
                <div class="matech-team">
                    <div class="icon-left">

                        @if($match->team_logo)
                            <img width="50" src="{{ asset('storage/logo/'.$match->team_logo)}}">
                        @endif
                    </div>
                    <div class="title">
                        <h4>{{ $match->team }}</h4>
                    </div>
                </div>

                <div class="clearfix"></div>
$
            </div>

            <div class="col-xs-12 col-sm-4 score">
                <span>VS</span>
            </div>

            <div class="col-xs-12 col-sm-4 matech-team-right">
                <div class="matech-team team-2">
                    <div class="title">
                        <h4>{{ $match->team2}}</h4>
                    </div>
                    <div class="icon-right">
                        @if($match->team2_logo)
                            <img width="50" src="{{ asset('storage/logo/'.$match->team2_logo)}}">
                        @endif
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="text-center">
            <h2 class="match-detail-league-title"><a href="#">{{ $match->started_at }}</a></h2>
        </div>
    </div> <!--container-->
</div>
<section class="match-details-section relative background-semi-dark" id="features">
    <div class="container">
        <div class="row section-separator">
            <div class="col-md-12">
                <h2 class="section-heading text-center match-details-title">Indok</h2>
                {!! $match->reason !!}
            </div>
            <div class="col-md-12">
                <h2 class="section-heading text-center match-details-title">Tippek</h2>
                {!! $match->tips !!}
            </div>
        </div>
    </div>
</section>
