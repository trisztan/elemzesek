@extends('frontend.frontend')
@section('header')
    @if (config("services.paypal.env") == 'sandbox')
        <script
            src="https://www.paypal.com/sdk/js?client-id={{config("services.paypal.sandbox_client_id")}}&currency=HUF"></script>
    @endif
    @if (config("services.paypal.env") == 'live')
        <script
            src="https://www.paypal.com/sdk/js?client-id={{config("services.paypal.live_client_id")}}&currency=HUF"></script>
    @endif

@endsection
@section('footer')
    <script>
        function getOrderData() {
            var data = $.ajax({
                url: '/payments/paypal/create',
                method: "GET",
                async: false,
                dataType: "json",
                cache: false,
                success: function (result) {
                    var data = result;
                },
                error: function (result) {
                    var data = result;
                }
            });

            return data.responseJSON;
        }

        paypal.Buttons({
            currency: "HUF",
            locale: "hu_HU",
            createOrder: function(data, actions) {
                return fetch('/payments/paypal/create', {
                    method: 'post'
                }).then(function(res) {
                    return res.json();
                }).then(function(data) {
                    return data.result.id;
                });
            },
            onCancel: function (data) {
                Swal.fire({
                    type: 'error',
                    title: 'A fizetés megszakadt',
                    text: 'Sajnos megszakadt a fizetés kérjük próbáld újra!',
                });
            },
            onApprove: function(data, actions) {
                return fetch('/payments/paypal/backref/' + data.orderID, {
                    method: 'post'
                }).then(function(res) {
                    return res.json();
                }).then(function(details) {
                    var status = details.result.status;
                    console.log(details.result);
                    if (status == 'COMPLETED') {
                        Swal.fire({
                            type: 'success',
                            title: 'Gratutálunk',
                            text: 'A fizetés sikeres volt és már prémium vagy!',
                            showCancelButton: false,
                            confirmButtonText: "Tovább az elemzésekhez!",
                        }).then((result) => {
                            window.location.replace('/');
                        });

                    } else if (status === 'CANCELED') {
                        Swal.fire({
                            type: 'error',
                            title: 'A fizetés megszakadt',
                            text: 'Sajnos megszakadt a fizetés kérjük próbáld újra!',
                        });

                    } else {
                        Swal.fire({
                            type: 'warning',
                            title: 'Figyelem!',
                            text: 'A fizetés még feldolgozás alatt van, kis türelmet. Ha párórán belül nem vagy prémium keresd az ügyfélszolgálatot e-mailben.',
                        });
                    }
                });
            }
        }).render('#paypal-button');

    </script>
@endsection
@section('content')

    <section class="features-section-1 relative background-semi-dark" id="features">
        <div class="container">
            <div class="row section-separator cup">
                <h2 class="text-center bold red">
                    <h2>
                        <h2 class="text-center bold">Mit tartalmazz az elemzés csomag?</h2>

                        <div class="row">
                            <div class="col-md-3">
                                <a href="{{ url('fizetes') }}" class="btn btn-lg btn-block btn-danger">Most
                                    csak {{ App\Utils\Price::orderPrice() }}Ft</a>
                                <img src="{{ url('images/cup.png') }}" class="img-responsive">

                            </div>
                            <div class="col-md-9">
                                <ul>
                                    <li>30 napos hozzáférés</li>
                                    <li>Minden napra részletes elemzés</li>
                                    <li>Havi 200-300 db elemzés</li>
                                    <li>Elemzéseinkhez tippeket is adunk</li>
                                    <li>Tippeink séma egyezés alapján készülnek</li>
                                    <li>Időt spórolunk neked</li>
                                </ul>
                            </div>
                        </div>
                        <hr/>
                        <div class="row">

                            {{--<div class="col-md-5 text-right">
                                <a href="{{ url('fizetes') }}" class="btn btn-lg btn-success"><i class="fa fa-credit-card"></i> Bankártyás fizetés</a>
                            </div>
                            <div class="col-md-2 text-center">
                               <h4>vagy</h4>
                            </div>--}}
                            <div class="col-md-6 text-right">
                                <p style="margin: 20px; font-size: 20px;"> Ha van Paypal fiókod! <i class="fa fa-chevron-right"></i></p>
                                <hr/>
                                <p style="margin: 20px; font-size: 20px;"> Ha bankártyával szeretnél fizetni! <i class="fa fa-chevron-right"></i></p>
                            </div>
                            <div class="col-md-4 text-center">
                                <div id="paypal-button"></div>
                            </div>
                        </div>

            </div>
        </div>
    </section>
@endsection
