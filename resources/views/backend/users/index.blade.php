@extends('backend.backend')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content mt-3">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Felhasználók</h3>

                                <div class="card-tools">
                                    <a href="{{ route('users.create') }}" class="btn btn-info btn-sm"><i class="fa fa-plus"></i> Új felhasználó</a>
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body p-1">
                                <table id="users" class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Név</th>
                                        <th>Keresztnév</th>
                                        <th>Vezetéknév</th>
                                        <th>Prémium</th>
                                        <th>E-mail</th>
                                        <th>Megerősítve</th>
                                        <th>Admin</th>
                                        <th width="1">#</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($data as $row)
                                        <tr>
                                            <td>{{$row->id}}</td>
                                            <td>{{$row->name}}</td>
                                            <td>{{$row->firstname}}</td>
                                            <td>{{$row->lastname}}</td>
                                            <td class="text-center">{!! !empty($row->access) ? '<button class="btn btn-sm btn-success"><i class="fa fa-check"></i></button>':'<i class="fa fa-red fa-times"></i>' !!}</td>
                                            <td>{{$row->email}}</td>
                                            <td class="text-center">{!! !empty($row->email_verified_at) ? '<button class="btn btn-sm btn-success"><i class="fa fa-check"></i></button>':'<i class="fa fa-red fa-times"></i>' !!}</td>
                                            <td class="text-center">{!! $row->is_admin == 1 ? '<button class="btn btn-sm btn-success"><i class="fa fa-check"></i></button>':'<button class="btn btn-sm btn-info">No</button>' !!}</td>
                                            <td class="text-center">
                                                <div class="row pull-right">
                                                    <div class="d-inline mr-1">
                                                        <a href="{{route('users.edit',['id'=>$row->id])}}"
                                                           class="btn btn-sm btn-info"><i class="fa fa-pencil"></i></a>
                                                    </div>
                                                    <div class="d-inline">
                                                        <form action="{{ route('users.destroy',['id'=>$row->id]) }}"
                                                              method="POST">
                                                            <input type="hidden" name="_method" value="DELETE">
                                                            <input type="hidden" name="_token"
                                                                   value="{{ csrf_token() }}">
                                                            <button type="submit"
                                                                    class="d-inline btn btn-sm btn-danger"><i
                                                                        class="fa fa-times"></i></button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
