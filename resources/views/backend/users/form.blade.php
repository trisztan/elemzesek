@extends('backend.backend')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content mt-3">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">
                                    @if(isset($user->id))
                                        Felhasználó szerkesztés
                                    @else
                                        Új felhasználó
                                    @endif
                                </h3>

                                <div class="card-tools">

                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body p-2">
                                @if(isset($user->id))
                                    {!! Form::model($user, ['method' => 'put', 'route' => ['users.update', $user->id]])  !!}
                                    <input type="hidden" name="id" value="{{$user->id}}">
                                @else
                                    {!! Form::model($user, ['method' => 'store', 'route' => ['users.store']])  !!}
                                @endif

                                <div class="form-group has-feedback @if ($errors->has('name')) has-error @endif">
                                    {{ Form::label('name', 'Felhasználónév') }}
                                    {!! Form::text('name', null, ['class' => 'form-control col-md-4', 'id' => 'name']) !!}
                                    @if ($errors->has('name'))
                                        <span class="help-block" role="alert">
                                                {{ $errors->first('name') }}
                                            </span>
                                    @endif
                                </div>


                                <div class="form-group has-feedback @if ($errors->has('firstname')) has-error @endif">
                                    {{ Form::label('firstname', 'Keresztnév') }}
                                    {!! Form::text('firstname', null, ['class' => 'form-control col-md-4', 'id' => 'firstname']) !!}
                                    @if ($errors->has('firstname'))
                                        <span class="help-block" role="alert">
                                            {{ $errors->first('firstname') }}
                                        </span>
                                    @endif
                                </div>


                                <div class="form-group has-feedback @if ($errors->has('lastname')) has-error @endif">
                                    {{ Form::label('lastname', 'Vezetéknév') }}
                                    {!! Form::text('lastname', null, ['class' => 'form-control col-md-4', 'id' => 'lastname']) !!}
                                    @if ($errors->has('lastname'))
                                        <span class="help-block" role="alert">
                                        {{ $errors->first('lastname') }}
                                    </span>
                                    @endif
                                </div>

                                <div class="form-group has-feedback @if ($errors->has('mobile')) has-error @endif">
                                    {{ Form::label('mobile', 'Mobil') }}
                                    {!! Form::text('mobile', null, ['class' => 'form-control col-md-4', 'id' => 'mobile']) !!}
                                    @if ($errors->has('mobile'))
                                        <span class="help-block" role="alert">
                                                    {{ $errors->first('mobile') }}
                                                </span>
                                    @endif
                                </div>

                                <div class="form-group has-feedback @if ($errors->has('email')) has-error @endif">
                                    {{ Form::label('email', 'E-mail cím') }}
                                    {!! Form::text('email', null, ['class' => 'form-control col-md-4', 'id' => 'email']) !!}
                                    @if ($errors->has('email'))
                                        <span class="help-block" role="alert">
                                                {{ $errors->first('email') }}
                                            </span>
                                    @endif
                                </div>
                                <div class="form-group has-feedback @if ($errors->has('password')) has-error @endif">
                                    {{ Form::label('password', 'Jelszó') }}
                                    {!! Form::password('password',['class' => 'form-control col-md-4', 'id' => 'password']) !!}
                                    @if ($errors->has('password'))
                                        <span class="help-block" role="alert">
                                            {{ $errors->first('password') }}
                                        </span>
                                    @endif
                                </div>
                                <div class="form-check">
                                    {!! Form::checkbox('is_admin')  !!}
                                    <label class="form-check-label" for="is_admin">Admin?</label>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Mentés</button>
                            </div>

                            {!! Form::close()  !!}
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div><!-- /.row -->
    </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
