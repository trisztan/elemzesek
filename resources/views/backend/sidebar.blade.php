<nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
        data-accordion="false">
        <li class="nav-item">
            <a href="{{route('users.index')}}" class="nav-link">
                <i class="nav-icon fa fa-users"></i>
                <p>
                    Felhasználók
                    <span class="badge badge-info right">{{\App\Models\User::count()}}</span>
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{route('matches.index')}}" class="nav-link">
                <i class="nav-icon fa fa-book"></i>
                <p>
                    Meccsek
                    <span class="badge badge-info right">{{\App\Models\Match::count()}}</span>
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{route('pages.index')}}" class="nav-link">
                <i class="nav-icon fa fa-book"></i>
                <p>
                    Oldalak
                    <span class="badge badge-info right">{{\App\Models\Page::count()}}</span>
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{route('orders.index')}}" class="nav-link">
                <i class="nav-icon fa fa-money"></i>
                <p>
                    Rendelések
                    <span class="badge badge-info right">{{\App\Models\Order::count()}}</span>
                </p>
            </a>
        </li>
        <hr/>
        <li class="nav-header">Beállítások</li>
        <li class="nav-item">
            <a href="{{route('settings.index')}}" class="nav-link">
                <i class="nav-icon fa fa-file"></i>
                <p>Beállítások</p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{route('ordertemplates.index')}}" class="nav-link">
                <i class="nav-icon fa fa-file"></i>
                <p>Rendelés sablonok</p>
            </a>
        </li>
    </ul>
</nav>
