@extends('backend.backend')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content mt-3">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">
                                    @if(isset($match->id))
                                        Meccs szerkesztés
                                    @else
                                        Új meccs
                                    @endif
                                </h3>

                                <div class="card-tools">

                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body p-2">
                                @if(isset($match->id))
                                    {!! Form::model($match, ['method' => 'put', 'route' => ['matches.update', $match->id],'enctype' => 'multipart/form-data'])  !!}
                                    <input type="hidden" name="id" value="{{$match->id}}">
                                @else
                                    {!! Form::model($match, ['method' => 'store', 'route' => 'matches.store','enctype' => 'multipart/form-data'])  !!}
                                @endif

                                <div class="form-group has-feedback @if ($errors->has('team')) has-error @endif">
                                    @if(isset($match->id) && $match->team_logo)
                                        <img width="50" src="{{ asset('storage/logo/'.$match->team_logo)}}">
                                    @endif
                                    {{ Form::label('team', 'Team') }}
                                    {!! Form::text('team', null, ['class' => 'form-control col-md-4', 'id' => 'team']) !!}
                                    @if ($errors->has('team'))
                                        <span class="help-block" role="alert">
                                                {{ $errors->first('team') }}
                                            </span>
                                    @endif
                                </div>

                                <div class="form-group has-feedback @if ($errors->has('team_logo')) has-error @endif">
                                    @if(isset($match->id) && $match->team2_logo)
                                        <img width="50" src="{{ asset('storage/logo/'.$match->team2_logo)}}">
                                    @endif
                                    {{Form::label('team_logo', 'Team logo',['class' => 'form-control col-md-4'])}}
                                    {{Form::file('team_logo')}}

                                    @if ($errors->has('team_logo'))
                                        <span class="help-block" role="alert">
                                        {{ $errors->first('team_logo') }}
                                    </span>
                                    @endif
                                </div>

                                <div class="form-group has-feedback @if ($errors->has('team2')) has-error @endif">
                                        {{ Form::label('team2', 'Team 2') }}
                                        {!! Form::text('team2', null, ['class' => 'form-control col-md-4', 'id' => 'team2']) !!}
                                        @if ($errors->has('team2'))
                                            <span class="help-block" role="alert">
                                            {{ $errors->first('team2') }}
                                        </span>
                                        @endif
                                    </div>
                                    <div class="form-group has-feedback @if ($errors->has('team2_logo')) has-error @endif">
                                        {{Form::label('team2_logo', 'Team 2 logo',['class' => 'form-control col-md-4'])}}
                                        {{Form::file('team2_logo')}}

                                        @if ($errors->has('team2_logo'))
                                            <span class="help-block" role="alert">
                                        {{ $errors->first('team2_logo') }}
                                    </span>
                                        @endif
                                    </div>
                                    <div class="form-group has-feedback @if ($errors->has('stadium')) has-error @endif">
                                        {{ Form::label('stadium', 'Stadion') }}
                                        {!! Form::text('stadium', null, ['class' => 'form-control col-md-4', 'id' => 'stadium']) !!}
                                        @if ($errors->has('stadium'))
                                            <span class="help-block" role="alert">
                                            {{ $errors->first('stadium') }}
                                        </span>
                                        @endif
                                    </div>
                                    <div class="form-group has-feedback @if ($errors->has('league')) has-error @endif">
                                        {{ Form::label('league', 'League') }}
                                        {!! Form::text('league', null, ['class' => 'form-control col-md-4', 'id' => 'league']) !!}
                                        @if ($errors->has('league'))
                                            <span class="help-block" role="alert">
                                            {{ $errors->first('league') }}
                                        </span>
                                        @endif
                                    </div>
                                    <div class="form-group has-feedback @if ($errors->has('reason')) has-error @endif">
                                        {{ Form::label('reason', 'Indok') }}
                                        {!! Form::textarea('reason', null, ['class' => 'summernote form-control col-md-4', 'id' => 'reason']) !!}
                                        @if ($errors->has('reason'))
                                            <span class="help-block" role="alert">
                                {{ $errors->first('reason') }}
                                </span>
                                        @endif
                                    </div>
                                    <div class="form-group has-feedback @if ($errors->has('tips')) has-error @endif">
                                        {{ Form::label('tips', 'Tippek') }}
                                        {!! Form::textarea('tips', null, ['class' => 'summernote form-control col-md-4', 'id' => 'tips']) !!}
                                        @if ($errors->has('tips'))
                                            <span class="help-block" role="alert">
                                {{ $errors->first('tips') }}
                                </span>
                                        @endif
                                    </div>
                                <div class="form-check">
                                    {!! Form::checkbox('free')  !!}
                                    <label class="form-check-label" for="free">Ingyenes?</label>
                                </div>
                                <div class="form-check">
                                    {!! Form::checkbox('status')  !!}
                                    <label class="form-check-label" for="status">Publikus?</label>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Mentés</button>
                            </div>

                            {!! Form::close()  !!}
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div><!-- /.row -->
    </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
