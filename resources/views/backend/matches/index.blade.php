@extends('backend.backend')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content mt-3">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Meccsek</h3>

                                <div class="card-tools">
                                    <a href="{{ route('matches.create') }}" class="btn btn-info btn-sm"><i class="fa fa-plus"></i> Új meccs</a>
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body p-1">
                                <table id="users" class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th>Team</th>
                                        <th>Team2</th>
                                        <th>Liga</th>
                                        <th>Stadion</th>
                                        <th>Elkezdve</th>
                                        <th>Ingyenes</th>
                                        <th>Publikus</th>
                                        <th width="1">#</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($data as $row)
                                        <tr>
                                            <td class="text-left">@if($row->team_logo)
                                                    <img width="50" src="{{ asset('storage/logo/'.$row->team_logo)}}">
                                                @endif
                                                {{$row->team}}
                                            </td>
                                            <td class="text-left">
                                         @if($row->team2_logo)
                                                    <img width="50" src="{{ asset('storage/logo/'.$row->team2_logo)}}">
                                                @endif{{$row->team2}}</td>
                                            <td>{{$row->league}}</td>
                                            <td>{{$row->stadium}}</td>
                                            <td>{{$row->started_at}}</td>
                                            <td class="text-center">{!! $row->free == 1 ? '<button class="btn btn-sm btn-success"><i class="fa fa-check"></i></button>':'<button class="btn btn-sm btn-info">Nem</button>' !!}</td>
                                            <td class="text-center">{!! $row->status == 1 ? '<button class="btn btn-sm btn-success"><i class="fa fa-check"></i></button>':'<button class="btn btn-sm btn-info">Nem</button>' !!}</td>
                                            <td class="text-center">
                                                <div class="row pull-right">
                                                    <div class="d-inline mr-1">
                                                        <a href="{{route('matches.edit',['id'=>$row->id])}}"
                                                           class="btn btn-sm btn-info"><i class="fa fa-pencil"></i></a>
                                                    </div>
                                                    <div class="d-inline">
                                                        <form action="{{ route('matches.destroy',['id'=>$row->id]) }}"
                                                              method="POST">
                                                            <input type="hidden" name="_method" value="DELETE">
                                                            <input type="hidden" name="_token"
                                                                   value="{{ csrf_token() }}">
                                                            <button type="submit"
                                                                    class="d-inline btn btn-sm btn-danger"><i
                                                                        class="fa fa-times"></i></button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
