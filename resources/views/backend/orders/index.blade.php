@extends('backend.backend')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content mt-3">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Rendelések</h3>

                                <div class="card-tools">
                                    <a href="{{ route('orders.create') }}" class="btn btn-info btn-sm"><i class="fa fa-plus"></i> Új rendelés</a>
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body p-1">
                                <table id="orders" class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Megrendelő</th>
                                        <th>Számlázási cím</th>
                                        <th>Állapot</th>
                                        <th>Összesen</th>
                                        <th>#</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($data as $row)
                                        <tr>
                                            <td>{{$row->order_id}}</td>
                                            <td>{{$row->lastname}} {{$row->firstname}}<br/>{{$row->mobile}}</td>
                                            <td>{{$row->country}} <br/>{{$row->zip}} {{$row->city}}<br/>{{$row->street}} </td>
                                            <td>{!! $row->status !!}</td>
                                            <td>{{$row->total_label ?? '---'}}</td>
                                            <td class="text-center">
                                                <div class="row pull-right">
                                                    <div class="d-inline">
                                                        <form action="{{ route('orders.destroy',['id'=>$row->id]) }}"
                                                              method="POST">
                                                            <input type="hidden" name="_method" value="DELETE">
                                                            <input type="hidden" name="_token"
                                                                   value="{{ csrf_token() }}">
                                                            <button type="submit"
                                                                    class="d-inline btn btn-sm btn-danger"><i
                                                                        class="fa fa-times"></i></button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
