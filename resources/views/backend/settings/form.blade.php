@extends('backend.backend')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content mt-3">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">
                                    Beállítások szerkesztése
                                </h3>

                                <div class="card-tools">

                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body p-2">
                                {!! Form::model($settings, ['method' => 'store', 'route' => 'settings.store'])  !!}

                                <div class="form-group has-feedback @if ($errors->has('gtm-id')) has-error @endif">
                                    {{ Form::label('price', 'Ár') }}
                                    {!! Form::text('price', \App\Models\Setting::getValue('price'), ['class' => 'form-control col-md-4', 'id' => 'gtm-id']) !!}
                                    @if ($errors->has('price'))
                                        <span class="help-block" role="alert">
                                            {{ $errors->first('price') }}
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group has-feedback @if ($errors->has('ga-id')) has-error @endif">
                                    {{ Form::label('saleprice', 'Akciós Ár') }}
                                    {!! Form::text('saleprice', \App\Models\Setting::getValue('saleprice'), ['class' => 'form-control col-md-4', 'id' => 'gtm-id']) !!}
                                    @if ($errors->has('saleprice'))
                                        <span class="help-block" role="alert">
                                            {{ $errors->first('saleprice') }}
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Mentés</button>
                            </div>
                            {!! Form::close()  !!}
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div><!-- /.row -->
    </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
