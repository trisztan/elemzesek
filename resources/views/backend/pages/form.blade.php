@extends('backend.backend')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content mt-3">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">
                                    @if(isset($page->id))
                                        Oldal szerkesztése
                                    @else
                                        Új olddal
                                    @endif
                                </h3>

                                <div class="card-tools">

                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body p-2">
                                @if(isset($page->id))
                                    {!! Form::model($page, ['method' => 'put', 'route' => ['pages.update', $page->id]])  !!}
                                    <input type="hidden" name="id" value="{{$page->id}}">
                                @else
                                    {!! Form::model($page, ['method' => 'store', 'route' => ['pages.store']])  !!}
                                @endif

                                <div class="form-group has-feedback @if ($errors->has('title')) has-error @endif">
                                    {{ Form::label('title', 'Cím') }}
                                    {!! Form::text('title', null, ['class' => 'form-control col-md-4', 'id' => 'title']) !!}
                                    @if ($errors->has('title'))
                                        <span class="help-block" role="alert">
                                                {{ $errors->first('title') }}
                                            </span>
                                    @endif
                                </div>

                                <div class="form-group has-feedback @if ($errors->has('slug')) has-error @endif">
                                    {{ Form::label('slug', 'Url') }}
                                    {!! Form::text('slug', null, ['class' => 'form-control col-md-4', 'id' => 'slug']) !!}
                                    @if ($errors->has('slug'))
                                        <span class="help-block" role="alert">
                                            {{ $errors->first('slug') }}
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group has-feedback @if ($errors->has('meta_title')) has-error @endif">
                                    {{ Form::label('meta_title', 'Meta Cím') }}
                                    {!! Form::text('meta_title', null, ['class' => 'form-control col-md-4', 'id' => 'meta_title']) !!}
                                    @if ($errors->has('meta_title'))
                                        <span class="help-block" role="alert">
                                        {{ $errors->first('meta_title') }}
                                    </span>
                                    @endif
                                </div>
                                <div
                                    class="form-group has-feedback @if ($errors->has('meta_description')) has-error @endif">
                                    {{ Form::label('meta_description', 'Meta leírás') }}
                                    {!! Form::text('meta_description', null, ['class' => 'form-control col-md-4', 'id' => 'meta_description']) !!}
                                    @if ($errors->has('meta_description'))
                                        <span class="help-block" role="alert">
                                    {{ $errors->first('meta_description') }}
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group has-feedback @if ($errors->has('description')) has-error @endif">
                                    {{ Form::label('description', 'Tartalom') }}
                                    {!! Form::textarea('description', null, ['class' => 'summernote form-control col-md-4', 'id' => 'description']) !!}
                                    @if ($errors->has('description'))
                                        <span class="help-block" role="alert">
                                {{ $errors->first('description') }}
                                </span>
                                    @endif
                                </div>
                                <div class="form-group has-feedback @if ($errors->has('widget')) has-error @endif">
                                    {{ Form::label('widget', 'Widget') }}
                                    {!! Form::select('widget',$widgets,null,['class' => 'form-control col-md-4', 'id' => 'widget']) !!}
                                    @if ($errors->has('widget'))
                                        <span class="help-block" role="alert">
                                    {{ $errors->first('widget') }}
                                </span>
                                    @endif
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Mentés</button>
                            </div>

                            {!! Form::close()  !!}
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
    <!-- /.content-wrapper -->
@endsection
