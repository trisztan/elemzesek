@extends('backend.backend')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content mt-3">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Oldalak</h3>

                                <div class="card-tools">
                                    <a href="{{ route('pages.create') }}" class="btn btn-info btn-sm"><i class="fa fa-plus"></i> Új oldal</a>
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body p-1">
                                <table id="pages" class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Cím</th>
                                        <th>Link</th>
                                        <th>Seo cím</th>
                                        <th>Seo leírás</th>
                                        <th>#</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($data as $row)
                                        <tr>
                                            <td>{{$row->id}}</td>
                                            <td>{{$row->title}}</td>
                                            <td>{{$row->slug}}</td>
                                            <td>{{$row->meta_title}}</td>
                                            <td>{{$row->meta_description}}</td>
                                            <td class="text-center">
                                                <div class="row pull-right">
                                                    <div class="d-inline mr-1">
                                                        <a href="{{route('pages.edit',['id'=>$row->id])}}"
                                                           class="btn btn-sm btn-info"><i class="fa fa-pencil"></i></a>
                                                    </div>
                                                    <div class="d-inline">
                                                        <form action="{{ route('pages.destroy',['id'=>$row->id]) }}"
                                                              method="POST">
                                                            <input type="hidden" name="_method" value="DELETE">
                                                            <input type="hidden" name="_token"
                                                                   value="{{ csrf_token() }}">
                                                            <button type="submit"
                                                                    class="d-inline btn btn-sm btn-danger"><i
                                                                        class="fa fa-times"></i></button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
