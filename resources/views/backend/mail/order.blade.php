<table class="panel" width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td class="panel-content">
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="panel-item">
                        <strong>Azonosító:</strong> #{{ $order->order_id}}
                    </td>
                </tr>
                <tr>
                    <td class="panel-item">
                        <strong>Megrendelő:</strong> {{ $order->firstname}} {{ $order->lastname}}
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<table class="panel" width="100%" cellpadding="0" cellspacing="0">
    <tbody><tr>
        <th class="text-center">Termék</th>
        <th class="text-center">Db</th>
        <th class="text-center">Ár</th>
        <th class="text-center">Összesen</th>
    </tr>
    @foreach($order->products as $product)
        <tr>
            <td>{{$product->sku}}<br/>{{$product->title}}</td>
            <td class="text-center">{{$product->qty}} DB</td>
            <td class="text-center">{{Helper::money($product->price)}}</td>
            <td class="text-center">{{Helper::money($product->qty*$product->price)}}</td>
        </tr>
    @endforeach
    </tbody>
</table>
<table class="panel" width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td class="panel-content">
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="panel-item">
                        <strong>Szállítás:</strong> {{$order->delivery}} {{$order->total_weight_label}} {{Helper::money($order->delivery_price)}}
                    </td>
                </tr>
                <tr>
                    <td class="panel-item">
                        <strong>Szállításkor fizetendő:</strong> <h2>{{$order->total_label}}</h2>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
