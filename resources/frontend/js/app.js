window.Popper = require('popper.js');
window.$ = window.jQuery = require('jquery');
require('bootstrap');
require("jquery-simple-timer")($); // passing jQuery as argument
var Inputmask = require('inputmask');
const Swal = require('sweetalert2');

var elemzesek = function () {
    'use strict';

    var setTooltip = function () {
        $('[data-toggle="tooltip"]').tooltip();
    };
    var flashMsg = function () {
        $('#flash-overlay-modal').modal();
        $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
    };
    var csrf = function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    };
    var zip = function () {
        Inputmask("9999").mask('#zip');
    }
    var mobile = function () {
        Inputmask("36-99-999-9999").mask('#mobile');
    }
    var profile = function(){
        var hash = window.location.hash;
        hash && $('a[href="' + hash + '"]').click();
    }
    var email = function(){
        Inputmask({
            mask: "*{1,20}[.*{1,20}][.*{1,20}][.*{1,20}]@*{1,20}[.*{2,6}][.*{1,2}]",
            greedy: false,
            onBeforePaste: function (pastedValue, opts) {
                pastedValue = pastedValue.toLowerCase();
                return pastedValue.replace("mailto:", "");
            },
            definitions: {
                '*': {
                    validator: "[0-9A-Za-z!#$%&'*+/=?^_`{|}~\-]",
                    casing: "lower"
                }
            }
        }).mask('#email');
    }
    var homeCounter = function () {
        $('.number').each(function () {
            $(this).prop('Counter', 0).animate({
                Counter: $(this).text()
            }, {
                duration: 4000,
                easing: 'swing',
                step: function (now) {
                    $(this).text(Math.ceil(now))
                }
            })
        })
    }
    var deviceControll = function () {
        if (windowSize('width') < 768) {
            $('body').removeClass('desktop').removeClass('tablet').addClass('mobile')
        } else if (windowSize('width') < 992) {
            $('body').removeClass('mobile').removeClass('desktop').addClass('tablet')
        } else {
            $('body').removeClass('mobile').removeClass('tablet').addClass('desktop')
        }
    }
    var windowSize = function (el) {
        var result = 0
        if (el == 'height') {
            result = window.innerHeight ? window.innerHeight : $(window).height()
        }
        if (el == 'width') {
            result = window.innerWidth ? window.innerWidth : $(window).width()
        }

        return result
    }
    return {

        init: function () {
            csrf();
            setTooltip();
            flashMsg();
            deviceControll();
            homeCounter();
            mobile();
            zip();
            email();
            profile();
        },
        resize: function () {
            deviceControll();
        }
    }
}();


$(document).ready(function () {
    elemzesek.init();
    $('.timer-quick').startTimer();
});

$(window).on('load', function () {
    $('.loader').fadeOut();
    $('.preloader').delay(200).fadeOut('slow');
    $('body').delay(200).css({
        'overflow': 'visible'
    });
});

$(window).on('resize', function () {
    elemzesek.resize();
});
