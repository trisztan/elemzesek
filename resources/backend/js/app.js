require('datatables.net-bs4');
require('datatables.net-responsive-bs4');
require('./AdminLTE');
require('summernote');

import Chart from "chart.js";

window.toastr = toastr;
var App = function () {
    'use strict';

    var verifyToken = function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    }
    var setDatatableDefaults = function() {
        $.extend( true, $.fn.dataTable.defaults, {
            oLanguage: {
                "sEmptyTable": "Nincs rendelkezésre álló adat",
                "sInfo": "Találatok: _START_ - _END_ Összesen: _TOTAL_",
                "sInfoEmpty": "Nulla találat",
                "sInfoFiltered": "(_MAX_ összes rekord közül szűrve)",
                "sInfoPostFix": "",
                "sInfoThousands": " ",
                "sLengthMenu": "_MENU_ találat oldalanként",
                "sLoadingRecords": "Betöltés...",
                "sProcessing": "Feldolgozás...",
                "sSearch": "Keresés:",
                "sZeroRecords": "Nincs a keresésnek megfelelő találat",
                "oPaginate": {
                    "sFirst": "Első",
                    "sPrevious": "Előző",
                    "sNext": "Következő",
                    "sLast": "Utolsó"
                },
                "oAria": {
                    "sSortAscending": ": aktiválja a növekvő rendezéshez",
                    "sSortDescending": ": aktiválja a csökkenő rendezéshez"
                }
            },
        } );
    }
    var backendUsers = function () {
        if ($.fn.DataTable.isDataTable($('#users'))) {
            $('#users').DataTable().destroy();
        }
        var users = $('#users').DataTable();
    }
    var backendPages = function () {
        if ($.fn.DataTable.isDataTable($('#pages'))) {
            $('#pages').DataTable().destroy();
        }
        var pages = $('#pages').DataTable();
    }
    var backendOrders = function () {
        if ($.fn.DataTable.isDataTable($('#orders'))) {
            $('#orders').DataTable().destroy();
        }
        var orders = $('#orders').DataTable();
    }
    var summernote = function () {
        $('.summernote').summernote({
            height: 300,
            lang: 'hu-HU',
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
                ['fontname', ['fontname']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ol', 'ul', 'paragraph', 'height']],
                ['table', ['table']],
                ['insert', ['link']],
                ['view', ['undo', 'redo', 'fullscreen', 'codeview', 'help']]
            ],
            popover: {
                image: [],
                link: [],
                air: []
            }
        });
    }
    var statistics = function () {
        if ($('#salesChart').length > 0) {
            $.ajax({
                url: '/admin/stats/',
                type: "GET",
                dataType: "json",
                cache: false,
                success: function (data) {
                    buildChart(data);
                }
            });
        }
    }
    var buildChart = function (data) {

        var salesChartCanvas = $('#salesChart').get(0).getContext('2d');
        // This will get the first returned node in the jQuery collection.
        var salesChart = new Chart(salesChartCanvas);

        var salesChartData = {
            labels: data.months,
            datasets: [
                {
                    label: 'Rendelés',
                    fillColor: 'rgba(0, 123, 255, 0.9)',
                    strokeColor: 'rgba(0, 123, 255, 1)',
                    pointColor: '#3b8bba',
                    pointStrokeColor: 'rgba(0, 123, 255, 1)',
                    pointHighlightFill: '#fff',
                    pointHighlightStroke: 'rgba(0, 123, 255, 1)',
                    data: data.results
                }
            ]
        }

        var salesChartOptions = {
            //Boolean - If we should show the scale at all
            showScale: true,
            //Boolean - Whether grid lines are shown across the chart
            scaleShowGridLines: false,
            //String - Colour of the grid lines
            scaleGridLineColor: 'rgba(0,0,0,.05)',
            //Number - Width of the grid lines
            scaleGridLineWidth: 1,
            //Boolean - Whether to show horizontal lines (except X axis)
            scaleShowHorizontalLines: true,
            //Boolean - Whether to show vertical lines (except Y axis)
            scaleShowVerticalLines: true,
            //Boolean - Whether the line is curved between points
            bezierCurve: true,
            //Number - Tension of the bezier curve between points
            bezierCurveTension: 0.3,
            //Boolean - Whether to show a dot for each point
            pointDot: false,
            //Number - Radius of each point dot in pixels
            pointDotRadius: 4,
            //Number - Pixel width of point dot stroke
            pointDotStrokeWidth: 1,
            //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
            pointHitDetectionRadius: 20,
            //Boolean - Whether to show a stroke for datasets
            datasetStroke: true,
            //Number - Pixel width of dataset stroke
            datasetStrokeWidth: 2,
            //Boolean - Whether to fill the dataset with a color
            datasetFill: true,
            //String - A legend template
            legendTemplate: '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].lineColor%>"></span><%=datasets[i].label%></li><%}%></ul>',
            //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
            maintainAspectRatio: false,
            //Boolean - whether to make the chart responsive to window resizing
            responsive: true,
            tooltipTemplate: "<%if (label){%><%=label %>: <%}%><%= value + ' db' %>",
        }

        //Create the line chart
        salesChart.Line(salesChartData, salesChartOptions)
    }
    return {
        init: function () {
            verifyToken();
            setDatatableDefaults();
            backendUsers();
            backendPages();
            backendOrders();
            summernote();
            statistics();
        }
    };
}();

$(function () {
    App.init();
});


