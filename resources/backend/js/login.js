global.$ = global.jQuery = require('jquery');

var App = function () {
    'use strict';

    var checkBox = function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass   : 'iradio_square-blue',
            increaseArea : '20%' // optional
        })
    }
    return {
        init: function () {
            checkBox();
        }
    };
}();

$(function () {
    App.init();
});
