#!/bin/sh
cd /var/www/vhosts/elemzesek.com/httpdocs

git fetch --all
git reset --hard origin/master
php artisan down

echo "Run Composer Install"
composer install
echo "Build frontend css & js"
yarn install
yarn run prod
echo "Clear cache"

php artisan cache:clear

echo "Run migrations"
php artisan migrate --force

echo "Fix permissions"
# Fix permissions
cd /var/www/vhosts/elemzesek.com/httpdocs
chown -R elemzesek:psacln .
chmod 777 -R storage
chmod -R guo+w storage
find . -type f -exec chmod 755 {} +
find . -type f -exec chmod 655 {} +

php artisan up
