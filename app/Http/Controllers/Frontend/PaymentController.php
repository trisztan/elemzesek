<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\User;
use App\Utils\Price;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Log;
use Auth;
use PayPalCheckoutSdk\Core\PayPalHttpClient;
use PayPalCheckoutSdk\Orders\OrdersCaptureRequest;
use PayPalCheckoutSdk\Core\SandboxEnvironment;
use PayPalCheckoutSdk\Core\ProductionEnvironment;
use PayPalCheckoutSdk\Orders\OrdersCreateRequest;

class PaymentController extends Controller
{

    public function pay()
    {
        if (!Auth::user()->check_profile) {
            flash('Az előfizetéshez kérjük töltsd ki a profilodat!')->warning();
            return redirect('profile');
        }
        return view('frontend.pay');
    }

    /**
     * Create Paypal Order
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function createPaypal()
    {
        $orderID = 'EM-'.rand(11111111,99999999);
        $price = Price::orderPrice();

        if (config("services.paypal.env") == 'sandbox') {
            $clientId = config("services.paypal.sandbox_client_id");
            $clientSecret = config("services.paypal.sandbox_client_secret");
            $environment = new SandBoxEnvironment($clientId, $clientSecret);
        }

        if (config("services.paypal.env") == 'live') {
            $clientId = config("services.paypal.live_client_id");
            $clientSecret = config("services.paypal.live_client_secret");
            $environment = new ProductionEnvironment($clientId, $clientSecret);
        }

        $client = new PayPalHttpClient($environment);
        $request = new OrdersCreateRequest();
        $request->prefer('return=representation');
        $request->body = [
            "intent" => "CAPTURE",
            "payer" => [
                "name"          => [
                    "given_name" => Auth::user()->firstname,
                    "surname"    => Auth::user()->lastname,
                ],
                "email_address" => Auth::user()->email,
                "address"       => [
                    "postal_code"    => Auth::user()->zip,
                    "admin_area_2"   => Auth::user()->city,
                    "address_line_1" => Auth::user()->street,
                    "country_code"   => "HU",
                ],
            ],
            "purchase_units"      => [
                [
                    "reference_id" => $orderID,
                    "amount"       => [
                        "value"         => $price,
                        "currency_code" => "HUF"
                    ]
                ]
            ],
            "application_context" => [
                "cancel_url" => "",
                "return_url" => ""
            ]
        ];

        try {
            $response = $client->execute($request);
            $paymentId = $response->result->id;

            if(Order::wherePaymentId($paymentId)->count()>0){
                return response()->json($response);
            }

            $order = new Order;
            $order->order_id = $orderID;
            $order->payment_id = $paymentId;
            $order->price = $price;
            $order->provider = 'paypal';
            $order->firstname = Auth::user()->firstname;
            $order->lastname = Auth::user()->lastname;
            $order->mobile = Auth::user()->mobile;
            $order->country = 'Magyarország';
            $order->status = 'pending';
            $order->accept = 1;
            $order->invoice = 0;
            $order->refund = 0;
            $order->email = Auth::user()->email;
            $order->user_id = Auth::user()->id;
            $order->zip = Auth::user()->zip;
            $order->city = Auth::user()->city;
            $order->street = Auth::user()->street;
            $order->created_at = date('Y-m-d H:i:s');
            $order->save();

            return response()->json($response);
        } catch (HttpException $ex) {
            return response()->json($ex->getMessage());
        }
    }

    public function getPaypal(Request $data)
    {
        Log::debug($data);
        $response = $this->getPaypalInformation($data->orderID);
        $id = $response->result->id;
        $payer_id = $response->result->payer->payer_id;  
        $status = $response->result->status;

        $zip = $response->result->purchase_units[0]->shipping->address->postal_code;
        $street = $response->result->purchase_units[0]->shipping->address->address_line_1;
        $city = $response->result->purchase_units[0]->shipping->address->admin_area_2;
        $fee = $response->result->purchase_units[0]->payments->captures[0]->seller_receivable_breakdown->paypal_fee->value;
        $phone = $response->result->payer->phone->phone_number->national_number;  

        $order = Order::wherePaymentId($id);
        
        if($order->count()>0)
        {
            $order = $order->first();

            if ($order->status == 'completed') {
                return response()->json(['status' => $order->status, 'orderid' => $data->payment_id]);
            }

            $order->city = $city;
            $order->zip = $zip;
            $order->street = $street;
            $order->status = strtolower($status);
            $order->mobile = preg_replace('/\D/', '',$phone);
            $order->fee = $fee;
            $order->update();

            $user = User::find($order->user_id);
            $user->city = $city;
            $user->zip = $zip;
            $user->street = $street;
            $user->mobile = $order->mobile;

            if ($order->status == 'completed') {
                $user->expired_at = User::expiredAt($user->expired_at);
                $user->updated_at = Carbon::now();
            }
            
            $user->save();
        }

        return response()->json($response);
    }

    /**
     * Get Paypal information by order id
     *
     * @param $orderID
     * @return bool|\BraintreeHttp\HttpResponse
     */
    private function getPaypalInformation($orderID)
    {
        if (config("services.paypal.env") == 'sandbox') {
            $clientId = config("services.paypal.sandbox_client_id");
            $clientSecret = config("services.paypal.sandbox_client_secret");
            $environment = new SandBoxEnvironment($clientId, $clientSecret);
        }

        if (config("services.paypal.env") == 'live') {
            $clientId = config("services.paypal.live_client_id");
            $clientSecret = config("services.paypal.live_client_secret");
            $environment = new ProductionEnvironment($clientId, $clientSecret);
        }

        $client = new PayPalHttpClient($environment);

        $request = new OrdersCaptureRequest($orderID);
        $request->prefer('return=representation');
        try {
            $response = $client->execute($request);
            return $response;
        } catch (HttpException $ex) {
            Log::debug($ex->getMessage());
            return response()->json($ex->getMessage());
            exit();
        }
    }
}
