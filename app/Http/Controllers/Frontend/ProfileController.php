<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Frontend\ProfileUpdateRequest;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use Auth;

class ProfileController extends Controller
{

    public function index(Request $request)
    {
        return view('frontend.profile');
    }

    /**
     * @param \App\Http\Requests\UpdateAccountRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(ProfileUpdateRequest $request)
    {
        $user = User::find(auth()->id());
        $user->name = $request->name;
        $user->email = $request->email;
        $user->firstname = $request->firstname;
        $user->lastname = $request->lastname;
        $user->zip = $request->zip;
        $user->city = $request->city;
        $user->street = $request->street;
        $user->mobile = str_replace('-', '', $request->mobile);
        $user->update();

        flash('Az adatok mentve!')->success();
        return redirect()->route('profile');
    }

    public function changePassword(Request $request)
    {
        $request->headers->set('referer', route('profile') . '#password');
        $user = User::find(Auth::user()->id);

        $this->validate($request, [
            'old_password' => [
                'required',
                function ($attribute, $value, $fail) use ($user) {
                    if (!\Hash::check($value, $user->password)) {
                        return $fail(__('The current password is incorrect.'));
                    }
                }
            ],
            'password'     => 'required|min:6|confirmed',
        ]);

        if (Hash::check($request->old_password, $user->password)) {
            $user->password = Hash::make($request->password);
            return $user->save();
        }

        flash('Az új jelszó mentve!')->success();
        return redirect()->route('profile');
    }

    public function delete(Request $request)
    {
        User::find(Auth::user()->id)->delete();
        $request->session()->flush();
        $request->session()->regenerate();
        flash('A profilod sikeresen törölted!')->success();
        return redirect('/');
    }
}
