<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\{User, Match};
use Carbon\Carbon;
use Illuminate\Http\Request;
use Auth;
use SEOMeta;
use OpenGraph;

class IndexController extends Controller
{
    public function index()
    {
        SEOMeta::addKeyword(['elemzések', 'tippek','legjobb oddsok']);
        OpenGraph::addProperty('og:image:type','image/jpeg');
        OpenGraph::addProperty('og:image:width',200);
        OpenGraph::addProperty('og:image:height',200);

        $usersSimple = User::count();
        $usersPremium = User::where('expired_at', '>', Carbon::now())->count();

        $matchesToday = count(Match::today());
        $matchesTotal = Match::count();
        $matches = Match::today();

        return view('frontend.home', compact('usersSimple', 'usersPremium', 'matchesToday', 'matchesTotal', 'matches'));
    }

    public function match(Request $request)
    {
        $match = Match::whereSlug($request->slug)->whereStatus(1);
        if ($match->count() == 0) {
            abort(404);
        }

        $match = $match->first();
        if($match->free == 0)
        {
            if(!Auth::check()){
                flash('A meccs megtekintéséhez regisztráció vagy bejelentkezés szükséges.')->warning();
                return back();
            }
            if(!Auth::user()->access){
                flash('A meccs megtekintéséhez előfizetés szükséges.')->warning();
                return redirect('elofizetes');
            }
        }
        $desc = str_limit(strip_tags ($match->reason), 50, ' (...)');
        SEOMeta::setTitle($match->title);
        SEOMeta::setDescription($desc);

        OpenGraph::setDescription($desc);
        OpenGraph::setTitle($match->title.' elemzés és tippek');
        OpenGraph::setUrl(url('elemzes/'.$match->slug));
        OpenGraph::addProperty('type', 'article');
        SEOMeta::addKeyword(['elemzések', 'tippek','legjobb oddsok']);
        SEOMeta::addMeta('article:published_time', $match->created_at->toW3CString(), 'property');
        OpenGraph::addProperty('og:image:type','image/jpeg');
        OpenGraph::addProperty('og:image:width',200);
        OpenGraph::addProperty('og:image:height',200);
        return view('frontend.match', compact('match'));
    }
}
