<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Storage;
use App\Models\Order;
use Carbon\Carbon;

class AdminController extends Controller
{
    /**
     * Show admin home page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function login()
    {
        return view('backend.login');
    }

    public function index()
    {
        $totalTraffic = Order::totalTraffic() ?? 0;
        $monthTraffic = Order::monthTraffic() ?? 0;
        $prevTraffic = Order::previousMonthTraffic() ?? 0;

        $totalOrders = Order::totalOrders() ?? 0;
        $monthOrders = Order::monthOrders() ?? 0;
        $prevOrders = Order::previousMonthOrders() ?? 0;
        $todayOrders = Order::todayOrders() ?? 0;

        return view('backend.index',
            compact('totalTraffic', 'monthTraffic', 'prevTraffic', 'totalOrders', 'monthOrders', 'prevOrders',
                'todayOrders'));
    }

    public function stats()
    {
        $months = [
            'Január',
            'Február',
            'Március',
            'Április',
            'Május',
            'Június',
            'Július',
            'Augusztus',
            'Szeptember',
            'Október',
            'November',
            'December'
        ];
        $results = [];
        $x = 1;
        while ($x <= 12) {
            $results[] = Order::whereStatus('completed')->whereRaw('MONTH(created_at) = '.$x)->count();
            $x++;
        }

        return response()->json(['months' => $months, 'results' =>$results]);
    }

    public function checkRemoteFile($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_NOBODY, 1);
        curl_setopt($ch, CURLOPT_FAILONERROR, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        if (curl_exec($ch) !== false) {
            return true;
        } else {
            return false;
        }
    }

    public function downloadImages()
    {
        $products = \App\Models\Product::select('image')->whereNotNull('image')->groupBy('image')->get();
        foreach ($products as $product) {
            $image = ltrim($product->image, '/');
            $url = 'https://www.motoralkatresznagyker.com/media/catalog/product/' . $image;
            if ($this->checkRemoteFile($url)) {
                $contents = file_get_contents($url);
                Storage::disk('local')->put('public/images/' . $image, $contents);
            }
        }
    }
}
