<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Page;
use App\Http\Requests\Backend\PageCreateRequest;
use App\Http\Requests\Backend\PageUpdateRequest;

class PageController extends Controller
{
    /**
     * Show pages
     *
     * @param \App\Models\Page $page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Page $page)
    {
        $data = $page->get();
        return view('backend.pages.index', compact('data'));
    }

    /**
     * Create page
     *
     * @param \App\Models\Page $page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Page $page)
    {
        $widgets = $page::getWidgets();
        return view('backend.pages.form', compact('page','widgets'));
    }

    /**
     * Store page
     *
     * @param \App\Http\Requests\Backend\PageCreateRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
*/
    public function store(PageCreateRequest $request)
    {
        $page = new Page;
        $page->title = $request->title;
        $page->slug = $request->slug;
        $page->meta_title = $request->meta_title;
        $page->meta_description = $request->meta_description;
        $page->description = $request->description;
        $page->widget = $request->widget;
        $page->save();

        return redirect(route('pages.index'));
    }

    /**
     * Edit page
     *
     * @param \App\Models\Page $page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Page $page)
    {
        $widgets = $page::getWidgets();

        return view('backend.pages.form', compact('page','widgets'));
    }

    /**
     * Update page
     *
     * @param \App\Http\Requests\Backend\PageUpdateRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(PageUpdateRequest $request)
    {
        $page = Page::find($request->id);
        $page->title = $request->title;
        $page->slug = $request->slug;
        $page->meta_title = $request->meta_title;
        $page->meta_description = $request->meta_description;
        $page->description = $request->description;
        $page->widget = $request->widget;
        $page->update();

        return redirect(route('pages.edit',$request->id));
    }

    /**
     * Destroy page
     *
     * @param \App\Models\Page $page
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Page $page)
    {
        $page->delete();
        return back();
    }
}
