<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\OrderTemplates;
use App\Http\Requests\Backend\OrderTemplateCreateRequest;
use App\Http\Requests\Backend\OrderTemplateUpdateRequest;

class OrderTemplateController extends Controller
{
    /**
     * Show order templates
     *
     * @param \App\Models\OrderTemplates $ordertemplate
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(OrderTemplates $ordertemplate)
    {
        $data = $ordertemplate->get();
        return view('backend.ordertemplates.index', compact('data'));
    }

    /**
     * Create template
     *
     * @param \App\Models\OrderTemplates $ordertemplate
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(OrderTemplates $ordertemplate)
    {
        return view('backend.ordertemplates.form', compact('ordertemplate'));
    }

    /**
     * Store template
     *
     * @param \App\Http\Requests\Backend\OrderTemplateCreateRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(OrderTemplateCreateRequest $request)
    {
        $ordertemplate = new OrderTemplates;
        $ordertemplate->title = $request->title;
        $ordertemplate->color = $request->color;
        $ordertemplate->email_title = $request->email_title;
        $ordertemplate->email_body = $request->email_body;
        $ordertemplate->save();

        return redirect(route('ordertemplates.index'));
    }

    /**
     * Edit page
     *
     * @param \App\Models\Page $ordertemplate
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(OrderTemplates $ordertemplate)
    {
        return view('backend.ordertemplates.form', compact('ordertemplate'));
    }

    /**
     * Update page
     *
     * @param \App\Http\Requests\Backend\PageUpdateRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(OrderTemplateUpdateRequest $request)
    {
        $ordertemplate = OrderTemplates::find($request->id);
        $ordertemplate->title = $request->title;
        $ordertemplate->color = $request->color;
        $ordertemplate->email_title = $request->email_title;
        $ordertemplate->email_body = $request->email_body;
        $ordertemplate->update();

        return redirect(route('ordertemplates.edit',$request->id));
    }

    /**
     * Destroy Order Template
     *
     * @param \App\Models\OrderTemplates $ordertemplate
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(OrderTemplates $ordertemplate)
    {
        $ordertemplate->status = 0;
        $ordertemplate->save();
        return back();
    }
}
