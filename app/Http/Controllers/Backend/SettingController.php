<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use App\Http\Requests\Frontend\SettingUpdateRequest;

class SettingController extends Controller
{
    /**
     * Show users
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Setting $setting)
    {
        $settings = $setting->get();
        return view('backend.settings.form', compact('settings'));
    }

    public function store(SettingUpdateRequest $request)
    {
        Setting::updateOrCreate(['key' => 'price'], ['value' => $request->price]);
        Setting::updateOrCreate(['key' => 'saleprice'], ['value' => $request->saleprice]);


        return redirect(route('settings.index'));
    }

}
