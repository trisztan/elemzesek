<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\OrderProducts;
use App\Models\OrderTemplates;
use App\Http\Requests\Backend\OrderCreateRequest;
use App\Http\Requests\Backend\OrderUpdateRequest;
use Illuminate\Http\Request;
use App\Notifications\OrderEmail;
use App\Models\OrderLogs;

class OrderController extends Controller
{
    /**
     * Show pages
     *
     * @param \App\Models\Order $order
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Order $order)
    {
        $data = $order->get();
        return view('backend.orders.index', compact('data'));
    }
    /**
     * Edit page
     *
     * @param \App\Models\Order $order
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Order $order)
    {
        $order = Order::find($order->id);
        return view('backend.orders.form', compact('order'));
    }

    /**
     * Update page
     *
     * @param \App\Http\Requests\Backend\PageUpdateRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(OrderUpdateRequest $request)
    {
        $order = Order::find($request->id);
        $template_id = $order->template_id;
        $order->firstname = $request->firstname;
        $order->lastname = $request->lastname;
        $order->email = $request->email;
        $order->phone = $request->phone;
        $order->company = $request->company;
        $order->taxnumber = $request->taxnumber;
        $order->country = $request->country;
        $order->city = $request->city;
        $order->zip = $request->zip;
        $order->street = $request->street;
        $order->delivery_firstname = $request->delivery_firstname;
        $order->delivery_lastname = $request->delivery_lastname;
        $order->delivery_phone = $request->delivery_phone;
        $order->delivery_taxnumber = $request->delivery_taxnumber;
        $order->delivery_country = $request->delivery_country;
        $order->delivery_city = $request->delivery_city;
        $order->delivery_zip = $request->delivery_zip;
        $order->delivery_street = $request->delivery_street;
        $order->delivery_company = $request->delivery_company;
        $order->template_id = $request->template_id;
        $order->update();

        if(isset($request->sendemail)){
            $order->notify(new OrderEmail($order->template_id,$order));
        }

        if($template_id != $order->template_id) {
            OrderLogs::create([
                'order_id'=>$request->id,
                'message'=>"A rendelés állapota: ".$order->status->title
            ]);
        }

        return redirect(route('orders.edit',$request->id));
    }

    /**
     * Destroy page
     *
     * @param \App\Models\Order $order
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Order $order)
    {
        $order->delete();
        return back();
    }
}
