<?php

namespace App\Http\Controllers\Backend;
use Storage;
use App\Http\Controllers\Controller;
use App\Models\Match;
use Auth;

class DownloadController extends Controller
{
    public function downloadImage($url)
    {
        $path = explode('.', basename($url));
        $path = str_slug($path[0]) . '.' . $path[1];
        $client = new \GuzzleHttp\Client();
        try {
            $resource = fopen(storage_path('app/public/logo/' . $path), 'w+');
            @$client->request('GET', $url, [
                'sink' => $resource
            ]);

            return basename(storage_path('app/public/logo/' . $path));
        } catch (\Exception $e) {
            return false;
        }
        return false;
    }

    public function setMatches()
    {
        $data = Storage::disk('local')->get('matches.json');
        $data = json_decode($data);
        $i=0;
        $matches = [];
        foreach ($data as $key => $match) {
            if ($match->status != 'publish' || Match::where('api_id', $match->id)->count() == 1) {
                continue;
            }
            
            if(!isset($match->the_meta->api_preview_json)){
                continue;
            }
            $api = json_decode($match->the_meta->api_preview_json[0]);
            if(empty($api->hometeam)){
                continue;
            }
            if($api == null){
                continue;
            }

            $matches[$key]['id'] = $match->id;
            $matches[$key]['slug'] = $match->slug;
            $matches[$key]['team'] = $api->hometeam;
            $matches[$key]['teamlogo'] = 'https://www.freesupertips.co.uk/wp-content/themes/freesupertips/image/team/' . $api->hometeam . '.png';
            $matches[$key]['team2'] = $api->awayteam;
            $matches[$key]['team2logo'] = 'https://www.freesupertips.co.uk/wp-content/themes/freesupertips/image/team/' . $api->awayteam . '.png';
            $matches[$key]['league'] = $match->the_meta->_league[0] ?? null;
            $matches[$key]['stadion'] = $match->the_meta->_stadium[0] ?? null;
            $matches[$key]['stats'][] = $match->the_meta->_headtohead[0] ?? null;
            $matches[$key]['stats'][] = $match->the_meta->_stats[0] ?? null;
            $matches[$key]['tips'][] = $match->the_meta->api_outcome_1[0] ?? null;
            $matches[$key]['tips'][] = $match->the_meta->api_outcome_2[0] ?? null;
            $matches[$key]['dateslug'] = $match->the_meta->_start_year[0] . "-" . $match->the_meta->_start_month[0] . "-" . $match->the_meta->_start_day[0];
            $matches[$key]['date'] = \Carbon\Carbon::parse($match->the_meta->_start_year[0] . "-" . $match->the_meta->_start_month[0] . "-" . $match->the_meta->_start_day[0] . " " . $match->the_meta->_kickoff[0] . ":00")
                ->addHour(1);
        }

        foreach ($matches as $matchdata)
        {
            $check = Match::where('api_id',$matchdata['id']);

            if($check->count()>0){
                continue;
            }

            $team_logo = $this->downloadImage($matchdata['teamlogo']);
            $team2_logo = $this->downloadImage($matchdata['team2logo']);

            $match = new Match();
            $match->api_id = $matchdata['id'];
            $match->user_id = Auth::user()->id;
            $match->team = $matchdata['team'];
            $match->team_logo = $team_logo;
            $match->team2 = $matchdata['team2'];
            $match->team2_logo = $team2_logo;
            $match->title = $match->team.' vs '.$match->team2.' '.$matchdata['dateslug'];
            $match->slug = str_slug($match->title);
            $match->reason = implode("<br/>", $matchdata['stats']);
            $match->tips = implode("<br/>", $matchdata['tips']);
            $match->started_at = $matchdata['date'];
            $match->stadium = $matchdata['stadion'] ?? null;
            $match->league = $matchdata['league'] ?? null;
            $match->free = 0;
            $match->status = 0;
            $match->save();
        }

    }

    public function index()
    {
        $this->download();
        $this->setMatches();

        return redirect('admin/matches');
    }

    public function download()
    {
        $url = 'https://www.freesupertips.co.uk/wp-json/wp/v2/previews/';
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', $url);

        Storage::disk('local')->put('matches.json', $res->getBody());
    }
}
