<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Http\Requests\Backend\UserCreateRequest;
use App\Http\Requests\Backend\UserUpdateRequest;
use Carbon\Carbon;

class UserController extends Controller
{
    /**
     * Show users
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(User $user)
    {
        $data = $user->get();
        return view('backend.users.index', compact('data'));
    }

    /**
     * Create user
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(User $user)
    {
        return view('backend.users.form', compact('user'));
    }

    /**
     * Store User
     *
     * @param \App\Http\Requests\Backend\UserCreateRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(UserCreateRequest $request)
    {
        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->firstname = $request->firstname;
        $user->lastname = $request->lastname;
        $user->is_admin = $request->is_admin ?? 0;
        $user->password = $request->password;
        $user->mobile = $request->mobile;
        $user->email_verified_at = Carbon::now();
        $user->save();

        return redirect(route('users.index'));
    }

    /**
     * Edit user
     *
     * @param \App\Models\User $user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(User $user)
    {
        return view('backend.users.form', compact('user'));
    }

    /**
     * Update user
     *
     * @param \App\Http\Requests\Backend\UserUpdateRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UserUpdateRequest $request)
    {
        $user = User::find($request->id);
        $user->name = $request->name;
        $user->firstname = $request->firstname;
        $user->lastname = $request->lastname;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->mobile = $request->mobile;
        $user->is_admin = $request->is_admin ?? 0;
        if (!empty($request->password)) {
            $user->password = $request->password;
        }
        $user->update();

        return redirect(route('users.edit',$request->id));
    }

    /**
     * Destroy user
     *
     * @param \App\Models\User $user
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(User $user)
    {
        $user->delete();
        return back();
    }
}
