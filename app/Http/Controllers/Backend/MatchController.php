<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Match;
use App\Http\Requests\Backend\MatchCreateRequest;
use App\Http\Requests\Backend\MatchUpdateRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;

class MatchController extends Controller
{
    /**
     * Show matches
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Match $match)
    {
        $data = $match->get();

        return view('backend.matches.index', compact('data'));
    }

    /**
     * Create Match
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Match $match)
    {
        return view('backend.matches.form', compact('match'));
    }

    /**
     * Store Match
     *
     * @param \App\Http\Requests\Backend\MatchCreateRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $match = new Match;
        $match->team = $request->team;

        if ($request->has('team_logo')) {
            $path = $request->file('team_logo')->store('public/logo');
            $match->team_logo = basename($path);
        }

        if ($request->has('team2_logo')) {
            $path = $request->file('team2_logo')->store('public/logo');
            $match->team2_logo = basename($path);
        }

        $match->team2 = $request->team2;
        $match->stadium = $request->stadium;
        $match->league = $request->league;
        $match->free = $request->free ?? 0;
        $match->status = $request->status ?? 0;
        $match->save();

        return redirect(route('matches.index'));
    }

    /**
     * Edit Match
     *
     * @param \App\Models\Match $match
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Match $match)
    {
        return view('backend.matches.form', compact('match'));
    }

    /**
     * Update Match
     *
     * @param \App\Http\Requests\Backend\MatchUpdateRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request)
    {
        //@todo: validalas
        $match = Match::find($request->id);
        $match->team = $request->team;
        $match->team2 = $request->team2;
        if ($request->has('team_logo')) {
            $path = $request->file('team_logo')->store('public/logo');
            $match->team_logo = basename($path);
        }

        if ($request->has('team2_logo')) {
            $path = $request->file('team2_logo')->store('public/logo');
            $match->team2_logo = basename($path);
        }
        $match->stadium = $request->stadium;
        $match->league = $request->league;
        $match->free = $request->free ?? 0;
        $match->status = $request->status ?? 0;
        $match->reason = $request->reason;
        $match->tips = $request->tips;
        $match->update();

        return redirect(route('matches.edit',$request->id));
    }

    /**
     * Destroy Match
     *
     * @param \App\Models\Match $match
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Match $match)
    {
        $match->delete();
        return back();
    }
}
