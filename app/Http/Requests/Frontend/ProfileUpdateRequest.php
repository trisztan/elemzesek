<?php

namespace App\Http\Requests\Frontend;

use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;

class ProfileUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $request->headers->set('referer', route('profile') . '#edit');

        return [
            'name'      => 'required|max:191',
            'firstname' => 'required|max:191',
            'lastname'  => 'required|max:191',
            'email'     => 'required|email|unique:users,email,' . auth()->id(),
            'zip'       => 'required|integer|digits:4',
            'city'      => 'required',
            'mobile'    => 'required',
            'street'    => 'required',
        ];
    }
}
