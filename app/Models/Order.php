<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Utils\Helper;
use App\Models\Delivery;
use Illuminate\Notifications\Notifiable;
use Carbon\Carbon;

class Order extends Model
{
    use Notifiable;

    protected $appends = ['total_label', 'total_weight_label'];

    public function logs()
    {
        return $this->hasMany('App\Models\OrderLogs', 'order_id', 'id')->orderBy('created_at', 'DESC');
    }

    public function calculateTotal()
    {
        return $this->price;
    }

    public function getTotalLabelAttribute()
    {
        return Helper::money($this->calculateTotal());
    }

    public static function totalTraffic()
    {
        if(self::count() == 0){
            return 0;
        }

        $total = 0;
        foreach (self::whereStatus('completed')->get() as $order) {
            $total += $order->calculateTotal();
        }
        return $total;
    }

    public static function totalOrders()
    {
        return self::whereStatus('completed')->count();
    }

    public static function todayOrders()
    {
        $date = Carbon::now();
        return self::whereDate('created_at', '>=', $date->today())->whereStatus('completed')->count();
    }

    public static function monthOrders()
    {
        $date = Carbon::now();
        return self::whereDate('created_at', '>=', $date->startOfMonth())->whereStatus('completed')->count();
    }

    public static function previousMonthOrders()
    {
        $last = Carbon::now();
        $current = Carbon::now();
        $lastMonth = $last->subMonth()->startOfMonth()->toDateTimeString();
        $currentMonth = $current->startOfMonth()->toDateTimeString();
        return self::whereBetween('created_at', [$lastMonth, $currentMonth])->whereStatus('completed')->count();
    }

    public static function monthTraffic()
    {
        $total = 0;
        $date = Carbon::now();
        $monthly = self::whereDate('created_at', '>=', $date->startOfMonth())->whereStatus('completed');
        if ($monthly->count() == 0) {
            return $total;
        }
        foreach ($monthly->get() as $order) {
            $total += $order->calculateTotal();
        }
        return $total;
    }

    public static function previousMonthTraffic()
    {
        $total = 0;
        $last = Carbon::now();
        $current = Carbon::now();
        $lastMonth = $last->subMonth()->startOfMonth()->toDateTimeString();
        $currentMonth = $current->startOfMonth()->toDateTimeString();
        $previously = self::whereBetween('created_at', [$lastMonth, $currentMonth])->whereStatus('completed');

        if ($previously->count() == 0) {
            return $total;
        }

        foreach ($previously->get() as $order) {
            $total += $order->calculateTotal();
        }

        return $total;
    }

    public function routeNotificationForMail($notification)
    {
        return $this->email;
    }
}
