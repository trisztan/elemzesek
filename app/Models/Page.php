<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    const ABOUT = 0;
    const LINK = 1;
    const MENU = 2;

    protected $appends = [
        'widget_label',
    ];

    /*
    |--------------------------------------------------------------------------
    | STATIC FUNCTIONS
    |--------------------------------------------------------------------------
    */
    /**
     * Get Types
     *
     * @return array
     */
    public static function getWidgets()
    {
        return [
            self::ABOUT => 'Footer (Rólunk)',
            self::LINK  => 'Footer (Hasznos)',
            self::MENU  => 'Header (Menü)',
        ];
    }
    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
    /**
     * @return mixed
     */
    public function getWidgetLabelAttribute()
    {
        return self::getWidgets()[$this->widget];
    }
}
