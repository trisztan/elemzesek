<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderLogs extends Model
{
    protected $fillable = ['order_id','message'];
}
