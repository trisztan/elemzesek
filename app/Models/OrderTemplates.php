<?php

 namespace App\Models;

 use Illuminate\Database\Eloquent\Model;

 class OrderTemplates extends Model
 {
     protected $appends = ['status_label'];

     public function getStatusLabelAttribute()
     {
         return '<button class="btn btn-info" style="background-color:'.$this->color.'">'.$this->title.'</button>';
     }
 }
