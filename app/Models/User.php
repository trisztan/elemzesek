<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Hash;
use Carbon\Carbon;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $fillable = ['name','firstname','lastname','email','password','zip','city','street','mobile'];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $appends = [
        'access',
        'access_days',
        'check_profile'
    ];
    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
    /**
     * Set password hash
     *
     * @param $password
     */
    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = Hash::needsRehash($password) ? Hash::make($password) : $password;
    }

    public static function expiredAt($expired_at)
    {
        $now = Carbon::now();

        if (Carbon::parse($expired_at)->timestamp < Carbon::parse($now)->timestamp) {
            return Carbon::now(config('app.timezone'))->addDays(30);
        }

        return Carbon::parse($expired_at)->addDays(30);
    }
     /**
     * Check Access Days
     *
     * @return int
     */
    public function getAccessDaysAttribute()
    {
        $now = Carbon::now();
        if (strtotime($this->expired_at) > strtotime(date('Y-m-d H:i:s'))) {
            return Carbon::parse($this->expired_at)->diffInDays($now) + 1;
        }
        return 0;
    }

    /**
     * Check Access
     *
     * @return bool
     */
    public function getAccessAttribute()
    {
        if (strtotime($this->expired_at) > strtotime(date('Y-m-d H:i:s'))) {
            return true;
        }

        return false;
    }

    /**
     * Check Address
     *
     * @return bool
     */
    public function getCheckProfileAttribute()
    {
        return (boolean)($this->zip && $this->city && $this->street && $this->mobile && $this->firstname && $this->lastname);
    }
}
