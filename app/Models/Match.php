<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
class Match extends Model
{
	protected $appends = [
        'started_at_second'
    ];
   /**
     * Get diff from now to started_at
     *
     * @return int
     */
    public function getStartedAtSecondAttribute()
    {
        $now = Carbon::now();

        if(empty($this->started_at) || (strtotime($now) > strtotime($this->started_at))){
            return 0;
        }

        return $now->diffInSeconds(Carbon::parse($this->started_at));
    }
    
    public static function today()
    {
        return self::whereBetween('started_at', [Carbon::today(), Carbon::tomorrow()->addDays(2)])
            ->whereStatus(1)
            ->orderByDesc('free')
            ->orderBy('started_at')
            ->get();
    }
}
