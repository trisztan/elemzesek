<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\Page;
use App\Models\Category;
use App\Models\Product;
use Cache;
use View;
use Schema;
use App\Models\Setting;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $headermenu = $footerabout = $footerlink = $categorymenu = $saleproducts = "";
        if(Schema::hasTable('pages')){
            $headermenu = Cache::remember('headermenu', 1, function () {
                return Page::whereWidget(Page::MENU)->get();
            });

            $footerabout = Cache::remember('footerabout', 1, function () {
                return Page::whereWidget(Page::ABOUT)->get();
            });

            $footerlink = Cache::remember('footerlink', 1, function () {
                return Page::whereWidget(Page::LINK)->get();
            });
        }
        if(Schema::hasTable('settings')) {
            $google_gtm_id = Cache::remember('google_gtm_id', 1, function () {
                return Setting::getValue('gtm-id') ?? null;
            });

            $google_ga_id = Cache::remember('google_ga_id', 1, function () {
                return Setting::getValue('ga-id') ?? null;
            });
        }
        View::share('headermenu',$headermenu);
        View::share('footerabout',$footerabout);
        View::share('footerlink',$footerlink);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
