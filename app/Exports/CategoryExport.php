<?php

namespace App\Exports;

use App\Models\Category;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\FromQuery;

class CategoryExport implements FromQuery, WithHeadings, WithMapping
{
    use Exportable;

    public function __construct($request)
    {
        $this->request = $request;
    }
    public function headings(): array
    {
        return [
            'part',
            'manufacturer',
            'brand',
            'ccm',
            'model',
            'year',
            'sku',
        ];
    }

    /**
     * @param mixed $category
     * @return array
     */
    public function map($category): array
    {
        return [
            $category->part,
            $category->manufacturer,
            $category->brand,
            $category->ccm,
            $category->model,
            $category->year,
            $category->sku,
        ];
    }

    public function query()
    {
        return Category::query()->limit(10000)->select('part','manufacturer','brand','ccm','model','year','sku');
    }

    public function chunkSize(): int
    {
        return 100;
    }
}
