<?php

namespace App\Utils;

class Helper
{
    public static function money($money)
    {
        return number_format((int)$money, 0, ',', ' ') . " Ft";
    }
}
