<?php
namespace App\Utils;
use App\Models\Setting;

class Price
{
    public static function orderPrice()
    {
        return Setting::getValue('saleprice') ?? Setting::getValue('price');
    }
}
