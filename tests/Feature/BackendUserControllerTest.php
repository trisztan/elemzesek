<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;

class BackendUserControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test admin users list page
     *
     * @return void
     */
    public function test_it_displays_the_admin_users_page()
    {
        $this->loginWithFakeAdmin();
        $response = $this->get('/admin/users');
        $response->assertStatus(200);
    }

    /**
     * Test create user page
     *
     * @return void
     */
    public function test_it_displays_the_admin_create_page()
    {
        $this->loginWithFakeAdmin();
        $response = $this->get('/admin/users/create');
        $response->assertStatus(200);
    }

    /**
     * Test edit user page
     *
     * @return void
     */
    public function test_it_displays_the_edit_page()
    {
        $this->loginWithFakeAdmin();
        $user = factory(User::class)->create();
        $response = $this->get("/admin/users/{$user->id}/edit");
        $response->assertStatus(200);
    }
}