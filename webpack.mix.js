const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.autoload({
    jquery: ['$', 'window.jQuery', 'jQuery','easy-autocomplete','easyAutocomplete'],
    easyAutocomplete: ['easy-autocomplete','easyAutocomplete'],
    summernote: ['summernote'],
    Dropzone : ['Dropzone'],
    toastr : ['toastr'],
    chart : ['chart'],
});

mix.js([
    './node_modules/jquery/dist/jquery.js',
    './node_modules/bootstrap/dist/js/bootstrap.min.js',
    './node_modules/summernote/dist/summernote-bs4.js',
    './node_modules/summernote/lang/summernote-hu-HU.js',
    './node_modules/jstree/dist/jstree.min.js',
    './node_modules/dropzone/dist/min/dropzone.min.js',
    './node_modules/toastr/build/toastr.min.js',
    './node_modules/chart.js/Chart.js',
    './resources/backend/js/app.js'],
    'public/backend/js/app.js')
    .sass('resources/backend/sass/app.scss', 'public/backend/css/');

mix.js([
        './node_modules/jquery/dist/jquery.js',
        './node_modules/bootstrap-sass/assets/javascripts/bootstrap.js',
        './node_modules/inputmask/dist/min/jquery.inputmask.bundle.min.js',
        './resources/frontend/js/app.js'],
    'public/frontend/js/app.js')
    .sass('resources/frontend/sass/app.scss', 'public/frontend/css/');

mix.js([
        './node_modules/jquery/dist/jquery.js',
        './node_modules/icheck/icheck.min.js',
        './resources/backend/js/login.js'],
    'public/backend/js/login.js');

mix.browserSync({
    host: 'elemzes.local',
    proxy: 'http://elemzes.local',
    port: 5666
});
