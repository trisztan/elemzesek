<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        // Default password
        $password = app()->environment('production') ? str_random() : '123456';
        $this->command->getOutput()->writeln("<info>Default user:</info> admin");
        $this->command->getOutput()->writeln("<info>Default password:</info> $password");

        // Create admin user
        $user = factory(User::class)->make([
            'name' => 'admin',
            'firstname' => 'Ferenc',
            'lastname' => 'Balogh',
            'mobile' => '+36304725776',
            'email' => 'baloghferenclevente@gmail.com',
            'password' => bcrypt($password),
            'is_admin' => 1,
        ]);
        $user->save();

        // 10 random users
        factory(User::class)->times(10)->create();
    }
}
