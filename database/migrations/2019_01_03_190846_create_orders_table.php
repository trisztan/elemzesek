<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('payment_id')->index()->nullable();
            $table->string('order_id')->index()->nullable();
            $table->integer('price');
            $table->integer('fee')->nullable();
            $table->string('provider');
            $table->string('firstname');
            $table->string('lastname');
            $table->string('email');
            $table->string('mobile');
            $table->string('country');
            $table->string('city');
            $table->string('zip');
            $table->string('street');
            $table->string('status')->default('pending');
            $table->tinyInteger('accept');
            $table->tinyInteger('invoice');
            $table->tinyInteger('refund');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
