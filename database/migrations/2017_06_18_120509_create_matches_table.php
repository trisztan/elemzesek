<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMatchesTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('matches', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('match_id')->nullable()->unsigned();
            $table->integer('api_id')->nullable()->unsigned();
            $table->integer('user_id')->nullable()->unsigned();
            $table->string('team')->nullable();
            $table->string('team_logo')->nullable();
            $table->string('team2')->nullable();
            $table->string('team2_logo')->nullable();
            $table->integer('visit')->nullable()->unsigned();
            $table->string('title')->nullable();
            $table->string('slug')->nullable();
            $table->string('league')->nullable();
            $table->string('stadium')->nullable();
            $table->text('reason')->nullable();
            $table->text('tips')->nullable();
            $table->tinyInteger('free')->default(0);
            $table->tinyInteger('status')->default(0);
            $table->timestamp('started_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('matches');
    }
}
